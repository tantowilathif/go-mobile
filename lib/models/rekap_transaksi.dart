import 'package:json_annotation/json_annotation.dart';

part 'rekap_transaksi.g.dart';

// ignore_for_file: non_constant_identifier_names

@JsonSerializable(explicitToJson: true)
class RekapTransaksi {
  RekapTransaksiItem? data;
  String? message;
  String? status;
  int? status_code;

  RekapTransaksi({
    this.data,
    this.message,
    this.status,
    this.status_code,
  });

  factory RekapTransaksi.fromJson(Map<String, dynamic> parsedJson) =>
      _$RekapTransaksiFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$RekapTransaksiToJson(this);
}

@JsonSerializable(explicitToJson: true)
class RekapTransaksiItem {
  int? code;
  String? message;
  MasterTransaksi? RekMasterTransaksi;
  List<ListRekapTransaksi>? LapdetailTransaksi;

  RekapTransaksiItem({
    this.code,
    this.message,
    this.RekMasterTransaksi,
    this.LapdetailTransaksi,
  });

  factory RekapTransaksiItem.fromJson(Map<String, dynamic> parsedJson) =>
      _$RekapTransaksiItemFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$RekapTransaksiItemToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MasterTransaksi {
  double? pemasukan;
  double? pengeluaran;
  double? saldo;

  MasterTransaksi({this.pemasukan, this.pengeluaran, this.saldo});

  factory MasterTransaksi.fromJson(Map<String, dynamic> parsedJson) =>
      _$MasterTransaksiFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$MasterTransaksiToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ListRekapTransaksi {
  int? id;
  String? tanggal;
  String? keterangan;
  String? tanggal_edit;
  int? pengeluaran;
  int? pemasukan;

  ListRekapTransaksi({
    this.id,
    this.tanggal,
    this.keterangan,
    this.tanggal_edit,
    this.pengeluaran,
    this.pemasukan,
  });

  factory ListRekapTransaksi.fromJson(Map<String, dynamic> parsedJson) =>
      _$ListRekapTransaksiFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$ListRekapTransaksiToJson(this);
}
