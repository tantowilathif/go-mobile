// ignore_for_file: must_be_immutable, prefer_final_fields

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/modules/profile/profile_controller.dart';
import 'package:starter_1/shared/methods.dart';
import 'package:starter_1/shared/themes.dart';
import 'package:validators/validators.dart';

class ProfileMasterView extends StatelessWidget {
  ProfileMasterView({Key? key}) : super(key: key);

  //* Text Editing Controllers
  TextEditingController _txtNameController = TextEditingController();
  TextEditingController _txtEmailController = TextEditingController();
  TextEditingController _txtUsernameController = TextEditingController();

  //* Text Field Focus Nodes
  final _txtNameFocusNode = FocusNode();
  final _txtEmailFocusNode = FocusNode();
  final _txtUsernameFocusNode = FocusNode();

  //* Variables
  final _formKey = GlobalKey<FormState>();

  late User userArgs;

  @override
  Widget build(BuildContext context) {
    ProfileController _profileController = Get.put(ProfileController());

    //* Methods (Function)
    _pickImage() {
      _profileController.chooseNewImageFromGallery();
    }

    _onClickSimpan() {
      if (_formKey.currentState!.validate()) {
        _formKey.currentState!.save();

        if (_txtNameController.text == userArgs.name &&
            _txtEmailController.text == userArgs.email &&
            _txtUsernameController.text == '' &&
            _profileController.avatarFile.value == null) {
          Get.snackbar('Alert', 'Tidak ada field yang di ubah');
        } else if (_txtNameController.text == userArgs.name &&
            _txtEmailController.text == userArgs.email &&
            _txtUsernameController.text == userArgs.username &&
            _profileController.avatarFile.value == null) {
          Get.snackbar('Alert', 'Tidak ada field yang di ubah');
        } else {
          User user = User(
            name: _txtNameController.text,
            username: _txtUsernameController.text,
            email: _txtEmailController.text,
          );

          _profileController.updateProfile(textValue: user, id: userArgs.id);
        }
      }
    }

    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return GetX<ProfileController>(
          initState: (_) {
            userArgs = Get.arguments;

            _txtNameController.text = userArgs.name ?? '...';
            _txtEmailController.text = userArgs.email ?? '';
            _txtUsernameController.text = userArgs.username ?? '';
          },
          builder: (_) {
            return Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: AppBar(
                centerTitle: true,
                title: Text('Update Profile'),
              ),
              body: WillPopScope(
                onWillPop: () async {
                  _.avatarFile.value = null;

                  return Future.value(true);
                },
                child: InkWell(
                  onTap: () {
                    FocusScope.of(context).unfocus();
                  },
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  focusColor: Colors.transparent,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: defaultHorizontalMargin,
                      vertical: defaultVerticalMargin,
                    ),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Spacer(),
                          Stack(
                            alignment: Alignment.bottomCenter,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                child: Container(
                                  width: sy(80),
                                  height: sy(80),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: greyColor.withOpacity(0.5),
                                    ),
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                      image: (_.avatarFile.value != null)
                                          ? FileImage(_.avatarFile.value!)
                                          : NetworkImage(
                                                  "http://${_.urlAvatar}")
                                              as ImageProvider,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 200,
                                top: 80,
                                child: Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: mainColor,
                                  ),
                                  height: sy(25),
                                  child: IconButton(
                                    splashRadius: 24,
                                    padding: EdgeInsets.all(2),
                                    onPressed: () {
                                      _pickImage();
                                    },
                                    icon: Icon(Icons.camera_alt_rounded),
                                    color: whiteColor,
                                    iconSize: 20,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: sy(20),
                          ),
                          TextFormField(
                            controller: _txtNameController,
                            focusNode: _txtNameFocusNode,
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (value) {
                              Methods.txtFieldFocusChange(
                                context,
                                _txtNameFocusNode,
                                _txtUsernameFocusNode,
                              );
                            },
                            validator: (value) {
                              if (isNull(value)) {
                                return 'Nama harus di isi';
                              } else if (value!.length <= 1) {
                                return 'Nama Invalid ';
                              }
                            },
                            decoration: InputDecoration(
                              alignLabelWithHint: true,
                              labelText: 'Nama',
                              hintText: 'Masukkan Nama anda',
                              isDense: true,
                              errorStyle: errorTextFont.copyWith(
                                fontSize: sy(8),
                                height: 0.6,
                              ),
                              enabledBorder: enabledBorder,
                              focusedBorder: focusedBorder,
                              errorBorder: errorBorder,
                              focusedErrorBorder: focusedError,
                              labelStyle: secondBlackTextFont,
                              floatingLabelStyle: secondBlackTextFont,
                              hintStyle:
                                  greyTextFont.copyWith(fontSize: sy(10)),
                            ),
                          ),
                          TextFormField(
                            controller: _txtUsernameController,
                            focusNode: _txtUsernameFocusNode,
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (value) {
                              FocusScope.of(context).unfocus();
                            },
                            validator: (value) {
                              if (value!.length < 6 && value.isNotEmpty) {
                                return 'Username Invalid ';
                              }
                            },
                            decoration: InputDecoration(
                              alignLabelWithHint: true,
                              labelText: 'Username',
                              hintText: 'Masukkan Username baru',
                              isDense: true,
                              errorStyle: errorTextFont.copyWith(
                                fontSize: sy(8),
                                height: 0.6,
                              ),
                              enabledBorder: enabledBorder,
                              focusedBorder: focusedBorder,
                              errorBorder: errorBorder,
                              focusedErrorBorder: focusedError,
                              labelStyle: secondBlackTextFont,
                              floatingLabelStyle: secondBlackTextFont,
                              hintStyle:
                                  greyTextFont.copyWith(fontSize: sy(10)),
                            ),
                          ),
                          TextFormField(
                            controller: _txtEmailController,
                            focusNode: _txtEmailFocusNode,
                            textInputAction: TextInputAction.done,
                            onFieldSubmitted: (value) {
                              FocusScope.of(context).unfocus();
                            },
                            validator: (value) {
                              if (isNull(value)) {
                                return 'Email harus di isi';
                              } else if (!isEmail(value!)) {
                                return 'Email Invalid ';
                              }
                            },
                            decoration: InputDecoration(
                              alignLabelWithHint: true,
                              labelText: 'Email',
                              hintText: 'Masukkan Email baru',
                              isDense: true,
                              errorStyle: errorTextFont.copyWith(
                                fontSize: sy(8),
                                height: 0.6,
                              ),
                              enabledBorder: enabledBorder,
                              focusedBorder: focusedBorder,
                              errorBorder: errorBorder,
                              focusedErrorBorder: focusedError,
                              labelStyle: secondBlackTextFont,
                              floatingLabelStyle: secondBlackTextFont,
                              hintStyle:
                                  greyTextFont.copyWith(fontSize: sy(10)),
                            ),
                          ),
                          SizedBox(
                            height: sy(40),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                              vertical: defaultVerticalMargin,
                              horizontal: defaultHorizontalMargin,
                            ),
                            height: sy(45),
                            width: double.infinity,
                            child: TextButton(
                              style: TextButton.styleFrom(
                                alignment: Alignment.center,
                                backgroundColor: mainColor,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                primary: whiteColor,
                              ),
                              child: (_.isLoadingUpdateProfile.value == false)
                                  ? Text(
                                      "Simpan",
                                      style: whiteTextFont.copyWith(
                                        fontSize: sy(15),
                                      ),
                                    )
                                  : SizedBox(
                                      height: sy(18),
                                      width: sy(18),
                                      child: CircularProgressIndicator(
                                        color: whiteColor,
                                      ),
                                    ),
                              onPressed: () {
                                if (_.isLoadingUpdateProfile.value == false) {
                                  _onClickSimpan();
                                }
                              },
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
