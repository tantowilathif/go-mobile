import 'package:json_annotation/json_annotation.dart';

part 'keuangan.g.dart';

// ignore_for_file: non_constant_identifier_names

@JsonSerializable(explicitToJson: true)
class Keuangan {
  KeuanganItem? data;
  String? message;
  String? status;
  int? status_code;

  Keuangan({
    this.data,
    this.message,
    this.status,
    this.status_code,
  });

  factory Keuangan.fromJson(Map<String, dynamic> parsedJson) =>
      _$KeuanganFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$KeuanganToJson(this);
}

@JsonSerializable(explicitToJson: true)
class KeuanganItem {
  String? tipe;
  String? tanggal_mulai;
  String? tanggal_selesai;
  List<double>? pemasukan;
  List<double>? pengeluaran;
  List<String>? label;
  int? total_items;

  KeuanganItem({
    this.tipe,
    this.tanggal_mulai,
    this.tanggal_selesai,
    this.pemasukan,
    this.pengeluaran,
    this.label,
    this.total_items,
  });

  factory KeuanganItem.fromJson(Map<String, dynamic> parsedJson) =>
      _$KeuanganItemFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$KeuanganItemToJson(this);
}
