// ignore_for_file: prefer_final_fields

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:starter_1/config/routes.dart';
import 'package:starter_1/modules/login/login_controller.dart';
import 'package:starter_1/shared/methods.dart';
import 'package:starter_1/shared/themes.dart';
import 'package:validators/validators.dart';

// ignore: must_be_immutable
class LoginView extends StatelessWidget {
  LoginView({Key? key}) : super(key: key);

  //* Text Editing Controllers
  TextEditingController _txtEmailUsernameController = TextEditingController();
  TextEditingController _txtPasswordController = TextEditingController();

  //* Text Field Focus Nodes
  final _txtEmailUsernameFocusNode = FocusNode();
  final _txtPasswordFocusNode = FocusNode();

  //* Variables
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    LoginController _loginController = Get.put(LoginController());

    _onClickLogin() async {
      var fcmToken = await FirebaseMessaging.instance.getToken();

      print('FAK == $fcmToken');

      if (_formKey.currentState!.validate()) {
        _formKey.currentState!.save();
        _loginController.login(
          emailorUsername: _txtEmailUsernameController.text,
          password: _txtPasswordController.text,
        );
      }
    }

    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          body: InkWell(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            focusColor: Colors.transparent,
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: defaultHorizontalMargin,
                vertical: defaultVerticalMargin,
              ),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Spacer(),
                    Container(
                      alignment: Alignment.center,
                      child: Image.asset(
                        "assets/images/qoins_logo2.png",
                        height: sy(80),
                        width: sx(260),
                      ),
                    ),
                    SizedBox(
                      height: sy(40),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Selamat datang kembali',
                        style: darkTextFont.copyWith(fontSize: sy(14)),
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Silahkan login untuk melanjutkan',
                        style: darkTextFont.copyWith(fontSize: sy(10)),
                      ),
                    ),
                    SizedBox(
                      height: sy(10),
                    ),
                    TextFormField(
                      controller: _txtEmailUsernameController,
                      focusNode: _txtEmailUsernameFocusNode,
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (value) {
                        Methods.txtFieldFocusChange(
                          context,
                          _txtEmailUsernameFocusNode,
                          _txtPasswordFocusNode,
                        );
                      },
                      validator: (value) {
                        if (isNull(value)) {
                          return 'Email atau Username harus di isi';
                        } else if (value!.length < 6) {
                          return 'Email atau Username Invalid ';
                        }
                      },
                      decoration: InputDecoration(
                        alignLabelWithHint: true,
                        labelText: 'Email atau Username',
                        hintText: 'Masukkan Email atau Password anda',
                        isDense: true,
                        errorStyle: errorTextFont.copyWith(
                          fontSize: sy(8),
                          height: 0.6,
                        ),
                        enabledBorder: enabledBorder,
                        focusedBorder: focusedBorder,
                        errorBorder: errorBorder,
                        focusedErrorBorder: focusedError,
                        labelStyle: secondBlackTextFont,
                        floatingLabelStyle: secondBlackTextFont,
                        hintStyle: greyTextFont.copyWith(fontSize: sy(10)),
                      ),
                    ),
                    TextFormField(
                      enableInteractiveSelection: false,
                      obscureText: true,
                      controller: _txtPasswordController,
                      focusNode: _txtPasswordFocusNode,
                      textInputAction: TextInputAction.done,
                      validator: (value) {
                        if (isNull(value)) {
                          return 'Password harus di isi';
                        } else if (value!.length < 6) {
                          return 'Password Invalid';
                        }
                      },
                      decoration: InputDecoration(
                        alignLabelWithHint: true,
                        labelText: 'Password',
                        hintText: 'Masukkan Password Anda',
                        isDense: true,
                        errorStyle: errorTextFont.copyWith(
                          fontSize: sy(8),
                          height: 0.6,
                        ),
                        enabledBorder: enabledBorder,
                        focusedBorder: focusedBorder,
                        errorBorder: errorBorder,
                        focusedErrorBorder: focusedError,
                        labelStyle: secondBlackTextFont,
                        floatingLabelStyle: secondBlackTextFont,
                        hintStyle: greyTextFont.copyWith(fontSize: sy(10)),
                      ),
                    ),
                    SizedBox(
                      height: sy(40),
                    ),
                    Obx(() => TextButton(
                          style: TextButton.styleFrom(
                            alignment: Alignment.center,
                            backgroundColor: mainColor,
                            elevation: 0,
                            fixedSize: Size(200, double.infinity),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                              side: BorderSide(color: Colors.transparent),
                            ),
                            primary: whiteColor,
                          ),
                          child: (_loginController.isLoadingLogin.value ==
                                  false)
                              ? Text(
                                  "Masuk",
                                  style:
                                      whiteTextFont.copyWith(fontSize: sy(15)),
                                )
                              : SizedBox(
                                  height: sy(18),
                                  width: sy(18),
                                  child: CircularProgressIndicator(
                                    color: whiteColor,
                                  ),
                                ),
                          onPressed: () {
                            if (_loginController.isLoadingLogin.value ==
                                false) {
                              _onClickLogin();
                            }
                          },
                        )),
                    Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Belum Punya Akun?',
                          style: highlightTextFont.copyWith(fontSize: sy(12)),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        GestureDetector(
                          onTap: () async {
                            FirebaseMessaging.instance.subscribeToTopic('all');

                            Get.toNamed(Routes.RegisterView);
                          },
                          child: Text(
                            'Register',
                            style: darkTextFont.copyWith(
                              fontSize: sy(12),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
