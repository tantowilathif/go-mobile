import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

//* Margins
const double defaultHorizontalMargin = 18;
const double defaultVerticalMargin = 10;

//* Colors Pallette
// Color mainColor = Color(0xFF2962FF);
// Color mainDarkColor = Color(0xFF040919);
// Color secondaryColor = Color(0xFFe3f2fd);
// Color blackColor = Color(0xFF08080D);
// Color secondBlackColor = Color(0xFF263238);
// Color greyColor = Color(0xFFABAAB0);
// Color whiteColor = Color(0xFFF2F2F2);
// Color errorColor = Color(0xFFD23A0D);
// Color highlightColor = Color(0xFF607d8b);
// Color greenColor = Color(0xFF1F9654);

Color mainColor = Color(0xFF38837c);
Color mainDarkColor = Color(0xFF040919);
Color mainDarkColor2 = Color(0xFF3b6f72);
Color secondaryColor = Color(0xFFe3f2fd);
Color blackColor = Color(0xFF08080D);
Color secondBlackColor = Color(0xFF263238);
Color greyColor = Color(0xFFABAAB0);
Color whiteColor = Color(0xFFF2F2F2);
Color errorColor = Color(0xFFD23A0D);
Color highlightColor = Color(0xFF069bab);
Color unHighlightColor = Color(0xFF85c9cd);
Color greenColor = Color(0xFF61b488);

//* Text Styles
var mainTextFont = GoogleFonts.poppins().copyWith(color: mainColor);
var secondaryTextFont = GoogleFonts.poppins().copyWith(color: secondaryColor);
var blackTextFont = GoogleFonts.poppins().copyWith(color: blackColor);
var secondBlackTextFont =
    GoogleFonts.poppins().copyWith(color: secondBlackColor);
var darkTextFont = GoogleFonts.poppins().copyWith(color: mainDarkColor);
var greyTextFont = GoogleFonts.poppins().copyWith(color: greyColor);
var greenTextFont = GoogleFonts.poppins().copyWith(color: greenColor);
var whiteTextFont = GoogleFonts.poppins().copyWith(color: whiteColor);
var highlightTextFont = GoogleFonts.poppins().copyWith(color: highlightColor);
var unHighlightTextFont =
    GoogleFonts.poppins().copyWith(color: unHighlightColor);
var errorTextFont = GoogleFonts.poppins().copyWith(color: errorColor);

//* Text Field Decoration (Enabled)
var enabledBorder = UnderlineInputBorder(
  borderSide: BorderSide(color: secondBlackColor, width: 1.0),
);

//* Text Field Decoration (Focused)
var focusedBorder = UnderlineInputBorder(
  borderSide: BorderSide(color: mainColor, width: 1.0),
);

//* Text Field Decoration (Error)
var errorBorder = UnderlineInputBorder(
  borderSide: BorderSide(color: errorColor, width: 1.0),
);

//* Text Field Decoration (Focused Error)
var focusedError = UnderlineInputBorder(
  borderSide: BorderSide(color: errorColor, width: 1.0),
);
