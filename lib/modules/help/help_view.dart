import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:starter_1/config/routes.dart';
import 'package:starter_1/modules/help/help_controller.dart';
import 'package:starter_1/shared/themes.dart';

class HelpView extends StatelessWidget {
  const HelpView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    HelpController _helpController = Get.put(HelpController());

    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return GetX<HelpController>(
          initState: (_) {},
          builder: (_) {
            return Scaffold(
              appBar: AppBar(
                title: Text('Bantuan'),
                centerTitle: true,
              ),
              body: Container(
                padding: EdgeInsets.symmetric(
                  vertical: defaultVerticalMargin,
                  horizontal: defaultHorizontalMargin,
                ),
                child: (_.isLoadingGetBantuanList.value == true)
                    ? Center(
                        child: SizedBox(
                          height: sy(20),
                          width: sy(20),
                          child: CircularProgressIndicator(
                            color: mainColor,
                          ),
                        ),
                      )
                    : ListView.separated(
                        itemCount: _.bantuanList.length,
                        separatorBuilder: (context, index) {
                          return Divider(
                            height: 0,
                            color: greyColor,
                            // thickness: 0.4,
                          );
                        },
                        itemBuilder: (context, index) {
                          return ListTile(
                            dense: true,
                            title: Text(
                              '${_.bantuanList[index].judul}',
                              style: darkTextFont.copyWith(
                                fontSize: sy(10),
                              ),
                            ),
                            subtitle: Text(
                              'Lihat Lebih Lengkap',
                              style: greyTextFont.copyWith(
                                fontSize: sy(8),
                              ),
                            ),
                            trailing: Icon(Icons.arrow_right_rounded),
                            onTap: () {
                              Get.toNamed(Routes.HelpDetailView,
                                  arguments: _.bantuanList[index]);
                            },
                          );
                        },
                      ),
              ),
            );
          },
        );
      },
    );
  }
}
