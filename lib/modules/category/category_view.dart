// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:starter_1/config/routes.dart';
import 'package:starter_1/modules/category/category_controller.dart';
import 'package:starter_1/modules/transaction/transaction_master_controller.dart';
import 'package:starter_1/shared/themes.dart';

class CategoryView extends StatelessWidget {
  CategoryView({Key? key}) : super(key: key);

  //* Variables
  bool? isFromMasuk;
  final TextEditingController _txtSearchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    CategoryController _categoryController = Get.put(CategoryController());

    TransactionMasterController transactionMasterController =
        Get.put(TransactionMasterController());

    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return GetX<CategoryController>(
          initState: (_) {
            isFromMasuk = Get.arguments;

            _categoryController.getCategoryList(
                isFromMasuk: isFromMasuk, searching: false, searchValue: '');
          },
          builder: (_) {
            return Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: AppBar(
                centerTitle: true,
                title: (isFromMasuk == true)
                    ? Text('Pilih Kategori Pemasukan')
                    : Text('Pilih Kategori Pengeluaran'),
                actions: [
                  IconButton(
                    onPressed: () {
                      Get.toNamed(Routes.CategoryMasterView,
                          arguments: [isFromMasuk, null, null, null]);
                    },
                    icon: Icon(
                      Icons.add_rounded,
                    ),
                    splashRadius: 20,
                  )
                ],
              ),
              body: Container(
                padding: EdgeInsets.symmetric(
                  vertical: defaultVerticalMargin,
                  horizontal: defaultHorizontalMargin,
                ),
                child: (_.isGotTimeOut.value == true)
                    ? Center(
                        child: TextButton(
                          onPressed: () {
                            _categoryController.getCategoryList(
                                isFromMasuk: isFromMasuk,
                                searching: false,
                                searchValue: '');
                          },
                          child: Text(
                            'Refresh',
                            style: mainTextFont.copyWith(
                              fontSize: sy(10),
                            ),
                          ),
                        ),
                      )
                    : InkWell(
                        onTap: () {
                          FocusScope.of(context).unfocus();
                        },
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        focusColor: Colors.transparent,
                        child: Column(
                          children: [
                            SizedBox(
                              height: sy(36),
                              child: TextField(
                                onChanged: (value) async {},
                                style: blackTextFont,
                                controller: _txtSearchController,
                                textInputAction: TextInputAction.search,
                                decoration: InputDecoration(
                                  labelText: "Cari Kategori",
                                  prefixIcon: Icon(Icons.search),
                                  focusColor: mainColor,
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10.0),
                                    ),
                                  ),
                                ),
                                onSubmitted: (value) {
                                  // _.searchList.clear();
                                  if (value.isEmpty) {
                                    _.getCategoryList(
                                      isFromMasuk: isFromMasuk,
                                      searching: false,
                                      searchValue: '',
                                    );
                                  } else {
                                    _.getCategoryList(
                                      isFromMasuk: isFromMasuk,
                                      searching: true,
                                      searchValue: _txtSearchController.text,
                                    );
                                  }
                                },
                              ),
                            ),
                            SizedBox(
                              height: sy(6),
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Klik Untuk Pilih Kategori',
                                style: darkTextFont.copyWith(fontSize: sy(12)),
                              ),
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                (_.isSearching.value == true)
                                    ? 'Hasil Pencarian'
                                    : 'Default Kategori',
                                style: greyTextFont.copyWith(fontSize: sy(10)),
                              ),
                            ),
                            (_.isLoadingGetCategoryList.value == true)
                                ? Expanded(
                                    child: Center(
                                      child: SizedBox(
                                        height: sy(20),
                                        width: sy(20),
                                        child: CircularProgressIndicator(
                                          color: mainColor,
                                        ),
                                      ),
                                    ),
                                  )
                                : (_.isSearching.value == true)
                                    ? Expanded(
                                        child: ListView.separated(
                                          itemCount: _.searchList.length,
                                          separatorBuilder: (context, index) {
                                            return Divider(
                                              height: 0,
                                              color: greyColor,
                                            );
                                          },
                                          itemBuilder: (context, index) {
                                            return ListTile(
                                              dense: true,
                                              title: Text(
                                                '${_.searchList[index].nama}',
                                                style: darkTextFont.copyWith(
                                                  fontSize: sy(10),
                                                ),
                                              ),
                                              onTap: () {
                                                if (isFromMasuk == true) {
                                                  transactionMasterController
                                                          .txtKategoriController2
                                                          .value
                                                          .text =
                                                      _.searchList[index].nama!;

                                                  transactionMasterController
                                                          .categoryItemMasuk =
                                                      _.searchList[index];

                                                  Get.back();
                                                } else {
                                                  transactionMasterController
                                                          .txtKategoriController
                                                          .value
                                                          .text =
                                                      _.searchList[index].nama!;

                                                  transactionMasterController
                                                          .categoryItemKeluar =
                                                      _.searchList[index];

                                                  Get.back();
                                                }
                                              },
                                            );
                                          },
                                        ),
                                      )
                                    : Column(
                                        children: [
                                          SizedBox(
                                            height: MediaQuery.of(context)
                                                        .size
                                                        .height /
                                                    2 -
                                                sy(90),
                                            child: ListView.separated(
                                              itemCount: (isFromMasuk == true)
                                                  ? _.categoryListDefaultMasuk
                                                      .length
                                                  : _.categoryListDefaultKeluar
                                                      .length,
                                              separatorBuilder:
                                                  (context, index) {
                                                return Divider(
                                                  height: 0,
                                                  color: greyColor,
                                                );
                                              },
                                              itemBuilder: (context, index) {
                                                return ListTile(
                                                  dense: true,
                                                  title: (isFromMasuk == true)
                                                      ? Text(
                                                          '${_.categoryListDefaultMasuk[index].nama}',
                                                          style: darkTextFont
                                                              .copyWith(
                                                            fontSize: sy(10),
                                                          ),
                                                        )
                                                      : Text(
                                                          '${_.categoryListDefaultKeluar[index].nama}',
                                                          style: darkTextFont
                                                              .copyWith(
                                                            fontSize: sy(10),
                                                          ),
                                                        ),
                                                  onTap: () {
                                                    if (isFromMasuk == true) {
                                                      transactionMasterController
                                                              .txtKategoriController2
                                                              .value
                                                              .text =
                                                          _
                                                              .categoryListDefaultMasuk[
                                                                  index]
                                                              .nama!;

                                                      transactionMasterController
                                                              .categoryItemMasuk =
                                                          _.categoryListDefaultMasuk[
                                                              index];

                                                      Get.back();
                                                    } else {
                                                      transactionMasterController
                                                              .txtKategoriController
                                                              .value
                                                              .text =
                                                          _
                                                              .categoryListDefaultKeluar[
                                                                  index]
                                                              .nama!;

                                                      transactionMasterController
                                                              .categoryItemKeluar =
                                                          _.categoryListDefaultKeluar[
                                                              index];

                                                      Get.back();
                                                    }
                                                  },
                                                );
                                              },
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: sy(8)),
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              'Custom Kategori',
                                              style: greyTextFont.copyWith(
                                                  fontSize: sy(10)),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(
                                              bottom: sy(8),
                                            ),
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              'Swipe left untuk update custom kategori',
                                              style: greyTextFont.copyWith(
                                                  fontSize: sy(8)),
                                            ),
                                          ),
                                          ConditionalSwitch.single<int>(
                                            context: context,
                                            valueBuilder:
                                                (BuildContext context) =>
                                                    (isFromMasuk == true)
                                                        ? _.categoryListMasuk
                                                            .length
                                                        : _.categoryListKeluar
                                                            .length,
                                            caseBuilders: {
                                              0: (BuildContext context) {
                                                return Center(
                                                  child: Text(
                                                    'No Data',
                                                    style: highlightTextFont
                                                        .copyWith(
                                                            fontSize: sy(10)),
                                                  ),
                                                );
                                              },
                                            },
                                            fallbackBuilder:
                                                (BuildContext context) {
                                              return SizedBox(
                                                height: MediaQuery.of(context)
                                                            .size
                                                            .height /
                                                        2 -
                                                    sy(100),
                                                child: ListView.separated(
                                                  itemCount:
                                                      (isFromMasuk == true)
                                                          ? _.categoryListMasuk
                                                              .length
                                                          : _.categoryListKeluar
                                                              .length,
                                                  separatorBuilder:
                                                      (context, index) {
                                                    return Divider(
                                                      height: 0,
                                                      color: greyColor,
                                                      // thickness: 0.4,
                                                    );
                                                  },
                                                  itemBuilder:
                                                      (context, index) {
                                                    return Slidable(
                                                      key: const ValueKey(0),
                                                      endActionPane: ActionPane(
                                                        extentRatio: 0.50,
                                                        motion:
                                                            const ScrollMotion(),
                                                        children: [
                                                          SlidableAction(
                                                            onPressed:
                                                                (context) async {
                                                              if (_.hasConnection
                                                                      .value ==
                                                                  true) {
                                                                await _
                                                                    .deleteCategory(
                                                                  idCategory: (isFromMasuk ==
                                                                          true)
                                                                      ? _
                                                                          .categoryListMasuk[
                                                                              index]
                                                                          .id
                                                                      : _
                                                                          .categoryListKeluar[
                                                                              index]
                                                                          .id,
                                                                )
                                                                    .whenComplete(
                                                                        () {
                                                                  if (isFromMasuk ==
                                                                      true) {
                                                                    _.categoryListMasuk
                                                                        .removeAt(
                                                                            index);
                                                                  } else {
                                                                    _.categoryListKeluar
                                                                        .removeAt(
                                                                            index);
                                                                  }
                                                                });
                                                              } else {
                                                                Get.snackbar(
                                                                    'Error',
                                                                    'Check your internet connection');
                                                              }
                                                            },
                                                            backgroundColor:
                                                                errorColor,
                                                            foregroundColor:
                                                                whiteColor,
                                                            icon: Icons.delete,
                                                            spacing: 5,
                                                            label: 'Delete',
                                                          ),
                                                          SlidableAction(
                                                            onPressed:
                                                                (context) {
                                                              Get.toNamed(
                                                                Routes
                                                                    .CategoryMasterView,
                                                                arguments: [
                                                                  isFromMasuk,
                                                                  true,
                                                                  (isFromMasuk ==
                                                                          true)
                                                                      ? _
                                                                          .categoryListMasuk[
                                                                              index]
                                                                          .nama
                                                                      : _
                                                                          .categoryListKeluar[
                                                                              index]
                                                                          .nama,
                                                                  (isFromMasuk ==
                                                                          true)
                                                                      ? _
                                                                          .categoryListMasuk[
                                                                              index]
                                                                          .id
                                                                      : _
                                                                          .categoryListKeluar[
                                                                              index]
                                                                          .id
                                                                ],
                                                              );
                                                            },
                                                            backgroundColor:
                                                                Colors
                                                                    .orangeAccent,
                                                            foregroundColor:
                                                                Colors.white,
                                                            icon: Icons
                                                                .edit_rounded,
                                                            spacing: 5,
                                                            label: 'Update',
                                                          ),
                                                        ],
                                                      ),
                                                      child: ListTile(
                                                        dense: true,
                                                        title:
                                                            (isFromMasuk ==
                                                                    true)
                                                                ? Text(
                                                                    '${_.categoryListMasuk[index].nama}',
                                                                    style: darkTextFont
                                                                        .copyWith(
                                                                      fontSize:
                                                                          sy(10),
                                                                    ),
                                                                  )
                                                                : Text(
                                                                    '${_.categoryListKeluar[index].nama}',
                                                                    style: darkTextFont
                                                                        .copyWith(
                                                                      fontSize:
                                                                          sy(10),
                                                                    ),
                                                                  ),
                                                        onTap: () {
                                                          if (isFromMasuk ==
                                                              true) {
                                                            transactionMasterController
                                                                    .txtKategoriController2
                                                                    .value
                                                                    .text =
                                                                _
                                                                    .categoryListMasuk[
                                                                        index]
                                                                    .nama!;

                                                            transactionMasterController
                                                                    .categoryItemMasuk =
                                                                _.categoryListMasuk[
                                                                    index];

                                                            Get.back();
                                                          } else {
                                                            transactionMasterController
                                                                    .txtKategoriController
                                                                    .value
                                                                    .text =
                                                                _
                                                                    .categoryListKeluar[
                                                                        index]
                                                                    .nama!;

                                                            transactionMasterController
                                                                    .categoryItemKeluar =
                                                                _.categoryListKeluar[
                                                                    index];

                                                            Get.back();
                                                          }
                                                        },
                                                      ),
                                                    );
                                                  },
                                                ),
                                              );
                                            },
                                          ),
                                        ],
                                      ),
                            Spacer(),
                          ],
                        ),
                      ),
              ),
            );
          },
        );
      },
    );
  }
}
