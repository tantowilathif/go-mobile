import 'package:dio/dio.dart';
import 'package:starter_1/models/transaksi.dart';
import 'package:starter_1/shared/constant.dart';

class TransactionMasterRepository {
  Future<void> createTransaksiRequest(
      {Transaksi? transaksiItem, String? token}) async {
    // Variables
    Response response;

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };

    BaseOptions options = BaseOptions(
      headers: headers,
      connectTimeout: 10000,
    );

    try {
      response = await Dio(options).post(
        "$ASYNC_API/transaksi",
        data: transaksiItem!.toJson(),
      );

      if (response.statusCode == 200) {
        print('[SUCCES] - Create Transaksi');
      }
    } on DioError catch (e) {
      if (e.type == DioErrorType.connectTimeout) {
        throw "Connection Timeout Coba Lagi";
      } else {
        print("[ERROR] - Create Transaksi == ${e.toString()}");

        throw e.toString();
      }
    }
  }
}
