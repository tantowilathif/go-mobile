import 'package:dio/dio.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/shared/constant.dart';

class ProfileRepository {
  //* Variables
  Dio dioService = Dio();

  Future<Auth?> updateRequest({User? user}) async {
    // Variables
    Response response;

    try {
      BaseOptions options = BaseOptions(connectTimeout: 10000);

      response = await Dio(options).put(
        "$API/user",
        data: user!.toJson(),
      );

      print("SAPI == ${response.data}");

      return Auth.fromJson(response.data);
    } on DioError catch (e) {
      print("[ERROR] - Update Profile == ${e.toString()}");

      if (e.type == DioErrorType.connectTimeout) {
        throw "Connection Timeout Coba Lagi";
      } else {
        Auth auth = Auth.fromJson(e.response!.data);

        throw auth.message!;
      }
    }
  }
}
