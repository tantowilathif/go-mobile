import 'dart:convert';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/models/category.dart';
import 'package:starter_1/modules/category/category_repository.dart';
import 'package:starter_1/utils/internet_connection_check.dart';

class CategoryController extends GetxController {
  //* Vairables for conditional view
  var isLoadingCreateCategory = false.obs;
  var isLoadingGetCategoryList = false.obs;
  var isSearching = false.obs;
  var selectedCategory = false.obs;
  var hasConnection = true.obs;
  var isGotTimeOut = false.obs;

  //* Other Variables
  final CategoryRepository _categoryRepository = CategoryRepository();

  Category? userCategory;

  RxList<CategoryItem> categoryListMasuk = <CategoryItem>[].obs;
  RxList<CategoryItem> categoryListKeluar = <CategoryItem>[].obs;
  RxList<CategoryItem> categoryListDefaultMasuk = <CategoryItem>[].obs;
  RxList<CategoryItem> categoryListDefaultKeluar = <CategoryItem>[].obs;
  RxList<CategoryItem> searchList = <CategoryItem>[].obs;
  RxList<CategoryItem> results = <CategoryItem>[].obs;

  //* Cycles
  @override
  void onInit() async {
    super.onInit();

    hasConnection.value =
        await InternetConnectionCheck.getInstance().checkConnectivity();

    InternetConnectionCheck.getInstance().connectionChange.listen(
      (event) {
        connectionChanged(event);
      },
    );

    if (hasConnection.value == false) {
      isGotTimeOut.value = true;
    }
  }

  //* Methods (Function)
  void connectionChanged(dynamic hasConnectionStatus) {
    hasConnection.value = hasConnectionStatus;
  }

  Future<void> getCategoryList(
      {bool? searching, bool? isFromMasuk, String? searchValue}) async {
    // Variables
    User? userData;

    isLoadingGetCategoryList.value = true;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    if (hasConnection.value == true) {
      try {
        userCategory = await _categoryRepository.listCategoryRequest(
          userId: userData.id,
          searchValue: searchValue,
        );

        jsonEncode('BB == ${userCategory!.data!.list}');

        if (searching == true) {
          if (isFromMasuk == true) {
            searchList.value = userCategory!.data!.list!
                .where((element) => element.tipe == 'masuk')
                .toList();
          } else {
            searchList.value = userCategory!.data!.list!
                .where((element) => element.tipe == 'keluar')
                .toList();
          }

          isSearching.value = true;
        } else {
          categoryListDefaultMasuk.value = userCategory!.data!.list!
              .where((element) =>
                  element.user_id == null && element.tipe == 'masuk')
              .toList();

          categoryListDefaultKeluar.value = userCategory!.data!.list!
              .where((element) =>
                  element.user_id == null && element.tipe == 'keluar')
              .toList();

          categoryListMasuk.value = userCategory!.data!.list!
              .where((element) =>
                  element.user_id != null && element.tipe == 'masuk')
              .toList();

          categoryListKeluar.value = userCategory!.data!.list!
              .where((element) =>
                  element.user_id != null && element.tipe == 'keluar')
              .toList();

          isSearching.value = false;
        }

        isLoadingGetCategoryList.value = false;
        isGotTimeOut.value = false;
      } catch (e) {
        if (e.toString() == "Connection Timeout Coba Lagi") {
          isGotTimeOut.value = true;

          Get.snackbar('Timeout', 'Silahkan Coba Lagi');
        } else {
          isLoadingGetCategoryList.value = false;

          Get.snackbar('Error', 'Something wrong please try again');
        }
      }
    } else {
      isLoadingGetCategoryList.value = false;

      Get.snackbar('Error', 'Check your internet connection');
    }

    update();
  }

  Future<void> createCategoryMasuk({String? categoryName}) async {
    // Variables
    User? userData;

    isLoadingCreateCategory.value = true;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    if (hasConnection.value == true) {
      try {
        CategoryItem categoryItemValue = CategoryItem(
          nama: categoryName,
          user_id: userData.id,
          tipe: 'masuk',
        );

        await _categoryRepository.createCategoryRequest(
          categoryItem: categoryItemValue,
          token: userData.token,
        );

        Get.back();

        isLoadingCreateCategory.value = false;

        await getCategoryList(
            isFromMasuk: true, searching: false, searchValue: '');
      } catch (e) {
        isLoadingCreateCategory.value = false;

        if (e.toString() == "Connection Timeout Coba Lagi") {
          Get.snackbar('Timeout', 'Silahkan Coba Lagi');
        } else {
          Get.snackbar('Error', 'Something wrong please try again');
        }
      }
    } else {
      isLoadingCreateCategory.value = false;

      Get.snackbar('Error', 'Check your internet connection');
    }
  }

  Future<void> createCategoryKeluar({String? categoryName}) async {
    // Variables
    User? userData;

    isLoadingCreateCategory.value = true;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    if (hasConnection.value == true) {
      try {
        CategoryItem categoryItemValue = CategoryItem(
          nama: categoryName,
          user_id: userData.id,
          tipe: 'keluar',
        );

        await _categoryRepository.createCategoryRequest(
          categoryItem: categoryItemValue,
          token: userData.token,
        );

        Get.back();

        isLoadingCreateCategory.value = false;

        await getCategoryList(
            isFromMasuk: false, searching: false, searchValue: '');
      } catch (e) {
        isLoadingCreateCategory.value = false;

        if (e.toString() == "Connection Timeout Coba Lagi") {
          Get.snackbar('Timeout', 'Silahkan Coba Lagi');
        } else {
          Get.snackbar('Error', 'Something wrong please try again');
        }
      }
    } else {
      isLoadingCreateCategory.value = false;

      Get.snackbar('Error', 'Check your internet connection');
    }
  }

  Future<void> updateCategory(
      {int? idCategory, String? categoryName, bool? isFromMasuk}) async {
    // Variables
    User? userData;

    isLoadingCreateCategory.value = true;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    if (hasConnection.value == true) {
      try {
        CategoryItem categoryItemValue = CategoryItem(
          id: idCategory,
          nama: categoryName,
          user_id: userData.id,
        );

        await _categoryRepository.updateCategoryRequest(
          categoryItem: categoryItemValue,
          token: userData.token,
        );

        Get.back();

        isLoadingCreateCategory.value = false;

        await getCategoryList(
            isFromMasuk: isFromMasuk, searching: false, searchValue: '');
      } catch (e) {
        isLoadingCreateCategory.value = false;

        if (e.toString() == "Connection Timeout Coba Lagi") {
          Get.snackbar('Timeout', 'Silahkan Coba Lagi');
        } else {
          Get.snackbar('Error', 'Something wrong please try again');
        }
      }
    } else {
      isLoadingCreateCategory.value = false;

      Get.snackbar('Error', 'Check your internet connection');
    }
  }

  Future<void> deleteCategory({int? idCategory}) async {
    // Variables
    User? userData;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    try {
      await _categoryRepository.deleteCategoryRequest(
        categoryId: idCategory,
        token: userData.token,
      );
    } catch (e) {
      Get.snackbar('Error', 'Something wrong please try again');
    }
  }
}
