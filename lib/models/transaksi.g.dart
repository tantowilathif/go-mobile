// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaksi.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Transaksi _$TransaksiFromJson(Map<String, dynamic> json) => Transaksi(
      id: json['id'] as int?,
      tipe: json['tipe'] as String?,
      jumlah_masuk: (json['jumlah_masuk'] as num?)?.toDouble(),
      jumlah_keluar: (json['jumlah_keluar'] as num?)?.toDouble(),
      tanggal: json['tanggal'] as String?,
      keterangan: json['keterangan'] as String?,
      kategori_id: json['kategori_id'] as int?,
      user_id: json['user_id'] as int?,
    );

Map<String, dynamic> _$TransaksiToJson(Transaksi instance) => <String, dynamic>{
      'id': instance.id,
      'tipe': instance.tipe,
      'jumlah_masuk': instance.jumlah_masuk,
      'jumlah_keluar': instance.jumlah_keluar,
      'tanggal': instance.tanggal,
      'keterangan': instance.keterangan,
      'kategori_id': instance.kategori_id,
      'user_id': instance.user_id,
    };

Transaksi2 _$Transaksi2FromJson(Map<String, dynamic> json) => Transaksi2(
      id: json['id'] as int?,
      tipe: json['tipe'] as String?,
      jumlah_masuk: (json['jumlah_masuk'] as num?)?.toDouble(),
      jumlah_keluar: (json['jumlah_keluar'] as num?)?.toDouble(),
      tanggal: json['tanggal'] as String?,
      tanggal_edit: json['tanggal_edit'] as String?,
      keterangan: json['keterangan'] as String?,
      kategori_id: json['kategori_id'] as int?,
      user_id: json['user_id'] as int?,
    );

Map<String, dynamic> _$Transaksi2ToJson(Transaksi2 instance) =>
    <String, dynamic>{
      'id': instance.id,
      'tipe': instance.tipe,
      'jumlah_masuk': instance.jumlah_masuk,
      'jumlah_keluar': instance.jumlah_keluar,
      'tanggal': instance.tanggal,
      'tanggal_edit': instance.tanggal_edit,
      'keterangan': instance.keterangan,
      'kategori_id': instance.kategori_id,
      'user_id': instance.user_id,
    };
