import 'package:dio/dio.dart';
import 'package:starter_1/models/category.dart';
import 'package:starter_1/shared/constant.dart';

class CategoryRepository {
  //* Variables
  Dio dioService = Dio();

  Future<Category?> listCategoryRequest(
      {int? userId, String? searchValue}) async {
    // Variables
    Response response;

    try {
      response = await Dio().get(
        '$API/kategori/list?filter={"nama": "$searchValue"}&user_id=$userId',
      );

      return Category.fromJson(response.data);
    } on DioError catch (e) {
      print("[ERROR] - Get List Category == ${e.toString()}");

      if (e.type == DioErrorType.connectTimeout) {
        throw "Connection Timeout Coba Lagi";
      } else {
        Category category = Category.fromJson(e.response!.data);

        throw category.message!;
      }
    }
  }

  Future<Category?> createCategoryRequest(
      {CategoryItem? categoryItem, String? token}) async {
    // Variables
    Response response;

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };

    BaseOptions options = BaseOptions(
      headers: headers,
      connectTimeout: 10000,
    );

    try {
      response = await Dio(options).post(
        "$ASYNC_API/kategori",
        data: categoryItem!.toJson(),
      );

      if (response.statusCode == 200) {
        print('[SUCCES] - Create Category');
      }
    } on DioError catch (e) {
      print("[ERROR] - Create Category == ${e.toString()}");

      if (e.type == DioErrorType.connectTimeout) {
        throw "Connection Timeout Coba Lagi";
      } else {
        Category category = Category.fromJson(e.response!.data);

        throw category.message!;
      }
    }
  }

  Future<Category?> updateCategoryRequest(
      {CategoryItem? categoryItem, String? token}) async {
    // Variables
    Response response;

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };

    BaseOptions options = BaseOptions(
      headers: headers,
      connectTimeout: 10000,
    );

    try {
      response = await Dio(options).put(
        "$API/kategori",
        data: categoryItem!.toJson(),
      );

      if (response.statusCode == 200) {
        print('[SUCCES] - Update Category');
      }
    } on DioError catch (e) {
      print("[ERROR] - Update Category == ${e.toString()}");

      if (e.type == DioErrorType.connectTimeout) {
        throw "Connection Timeout Coba Lagi";
      } else {
        Category category = Category.fromJson(e.response!.data);

        throw category.message!;
      }
    }
  }

  Future<Category?> deleteCategoryRequest(
      {int? categoryId, String? token}) async {
    // Variables
    Response response;

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };

    BaseOptions options = BaseOptions(
      headers: headers,
    );

    try {
      response = await Dio(options).delete(
        "$API/kategori/$categoryId",
      );

      if (response.statusCode == 200) {
        print('[SUCCES] - Delete Category');
      }
    } on DioError catch (e) {
      print("[ERROR] - Delete Category == ${e.toString()}");

      throw e.toString();
    }
  }
}
