import 'package:json_annotation/json_annotation.dart';

part 'bantuan.g.dart';

// ignore_for_file: non_constant_identifier_names

@JsonSerializable(explicitToJson: true)
class Bantuan {
  BantuanList? data;
  String? message;
  String? status;
  int? status_code;

  Bantuan({
    this.data,
    this.message,
    this.status,
    this.status_code,
  });

  factory Bantuan.fromJson(Map<String, dynamic> parsedJson) =>
      _$BantuanFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$BantuanToJson(this);
}

@JsonSerializable(explicitToJson: true)
class BantuanList {
  int? total_items;
  List<BantuanItem>? list;

  BantuanList({
    this.total_items,
    this.list,
  });

  factory BantuanList.fromJson(Map<String, dynamic> parsedJson) =>
      _$BantuanListFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$BantuanListToJson(this);
}

@JsonSerializable(explicitToJson: true)
class BantuanItem {
  int? id;
  String? judul;
  String? keterangan;
  String? link_v;
  String? link_youtube;

  BantuanItem({
    this.id,
    this.judul,
    this.keterangan,
    this.link_v,
    this.link_youtube,
  });

  factory BantuanItem.fromJson(Map<String, dynamic> parsedJson) =>
      _$BantuanItemFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$BantuanItemToJson(this);
}
