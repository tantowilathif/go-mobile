import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:starter_1/modules/home/home_controller.dart';
import 'package:starter_1/modules/profile/profile_controller.dart';
import 'package:starter_1/modules/profile/profile_view.dart';
import 'package:starter_1/modules/transaction/transaction_view.dart';
import 'package:starter_1/shared/themes.dart';

// ignore: must_be_immutable
class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  LineTouchData get lineTouchData1 => LineTouchData(
        handleBuiltInTouches: true,
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: greyColor.withOpacity(0.5),
        ),
      );

  FlGridData get gridData => FlGridData(
        show: true,
        drawHorizontalLine: true,
        drawVerticalLine: true,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: greyColor,
            strokeWidth: 0.5,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: greyColor,
            strokeWidth: 0.5,
          );
        },
      );

  FlBorderData get borderData => FlBorderData(
        show: true,
        border: Border(
          bottom: BorderSide(color: mainDarkColor, width: 1.5),
          left: BorderSide(color: mainDarkColor, width: 1.5),
          right: BorderSide(color: Colors.transparent),
          top: BorderSide(color: Colors.transparent),
        ),
      );

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    HomeController _homeController = Get.put(HomeController());
    // ignore: unused_local_variable
    ProfileController _profileController = Get.put(ProfileController());

    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return GetX<HomeController>(
          initState: (_) {
            _.controller!.isHarianPicked.value = true;
            _.controller!.filterStatus(0);
          },
          builder: (_) {
            return Scaffold(
              bottomNavigationBar: BottomNavigationBar(
                backgroundColor: mainDarkColor2,
                iconSize: 26,
                selectedFontSize: sy(10),
                selectedItemColor: whiteColor,
                selectedLabelStyle: highlightTextFont.copyWith(
                  fontWeight: FontWeight.bold,
                ),
                unselectedFontSize: sy(10),
                unselectedItemColor: unHighlightColor,
                unselectedLabelStyle: unHighlightTextFont.copyWith(
                  fontWeight: FontWeight.normal,
                ),
                currentIndex: _.selectedIndexPage.value,
                onTap: (index) => _.onBottomItemNavTapped(index),
                items: const <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                    icon: Icon(Icons.home_rounded),
                    label: 'Home',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.article_outlined),
                    label: 'Transaction',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.person_rounded),
                    label: 'Profile',
                  ),
                ],
              ),
              body: SafeArea(
                child: IndexedStack(
                  index: _.selectedIndexPage.value,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: defaultHorizontalMargin,
                        vertical: defaultVerticalMargin,
                      ),
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Report Grafik',
                              style: darkTextFont.copyWith(fontSize: sy(12)),
                            ),
                          ),
                          SizedBox(
                            height: sy(8),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Material(
                                  color: (_.isHarianPicked.value)
                                      ? highlightColor
                                      : null,
                                  borderRadius: BorderRadius.circular(8),
                                  elevation: (_.isHarianPicked.value) ? 4 : 1,
                                  child: InkWell(
                                    splashColor: greyColor.withOpacity(0.2),
                                    onTap: () {
                                      _.isHarianPicked.value = true;
                                      _.filterStatus(0);
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: (_.isHarianPicked.value)
                                            ? null
                                            : Border.all(color: greyColor),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      alignment: Alignment.center,
                                      padding: EdgeInsets.all(6),
                                      child: Text(
                                        'Minggu Ini',
                                        style: (_.isHarianPicked.value)
                                            ? whiteTextFont.copyWith(
                                                fontSize: sy(10),
                                                fontWeight: FontWeight.normal,
                                              )
                                            : darkTextFont.copyWith(
                                                fontSize: sy(10),
                                                fontWeight: FontWeight.normal,
                                              ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: sx(6),
                              ),
                              Expanded(
                                child: Material(
                                  color: (_.isMingguanPicked.value)
                                      ? highlightColor
                                      : null,
                                  borderRadius: BorderRadius.circular(8),
                                  elevation: (_.isMingguanPicked.value) ? 4 : 1,
                                  child: InkWell(
                                    splashColor: greyColor.withOpacity(0.2),
                                    onTap: () {
                                      _.isMingguanPicked.value = true;
                                      _.filterStatus(1);
                                      _.getLeftTitles(10, 5);
                                    },
                                    child: Container(
                                      alignment: Alignment.center,
                                      padding: EdgeInsets.all(6),
                                      decoration: BoxDecoration(
                                        border: (_.isMingguanPicked.value)
                                            ? null
                                            : Border.all(color: greyColor),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: Text(
                                        'Bulan Ini',
                                        style: (_.isMingguanPicked.value)
                                            ? whiteTextFont.copyWith(
                                                fontSize: sy(10),
                                                fontWeight: FontWeight.normal,
                                              )
                                            : darkTextFont.copyWith(
                                                fontSize: sy(10),
                                                fontWeight: FontWeight.normal,
                                              ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: sy(8),
                          ),
                          (_.isLoadingGetLaporanKeuangan.value == true)
                              ? Center(
                                  child: SizedBox(
                                    height: sy(20),
                                    width: sy(20),
                                    child: CircularProgressIndicator(
                                      color: mainColor,
                                    ),
                                  ),
                                )
                              : (_.isGotTimeOutLine.value == true)
                                  ? Center(
                                      child: TextButton(
                                        onPressed: () {
                                          _.isHarianPicked.value = true;
                                          _.filterStatus(0);
                                        },
                                        child: Text(
                                          'Refresh',
                                          style: mainTextFont.copyWith(
                                            fontSize: sy(10),
                                          ),
                                        ),
                                      ),
                                    )
                                  : Container(
                                      width: double.infinity,
                                      height:
                                          MediaQuery.of(context).size.height /
                                                  2 -
                                              sy(78),
                                      decoration: BoxDecoration(
                                        color: greyColor.withOpacity(0.2),
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Column(
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                  right: 15.0,
                                                  left: 8.0,
                                                  top: 15.0,
                                                  bottom: 8.0),
                                              child: (_.maxY.value == 0)
                                                  ? Center(
                                                      child: Text(
                                                        'No Data',
                                                        style: TextStyle(
                                                          color:
                                                              secondBlackColor,
                                                          fontSize: sy(8),
                                                        ),
                                                      ),
                                                    )
                                                  : LineChart(
                                                      LineChartData(
                                                        lineTouchData:
                                                            lineTouchData1,
                                                        gridData: gridData,
                                                        titlesData:
                                                            _.titlesData1,
                                                        borderData: borderData,
                                                        lineBarsData:
                                                            _.lineChartBarData1,
                                                        minX: 0,
                                                        maxX: _.maxX.value,
                                                        maxY: _.maxY.value,
                                                        minY: 0,
                                                      ),
                                                    ),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                '${_.laporanKeuanganLine!.data!.tanggal_mulai}  -  ${_.laporanKeuanganLine!.data!.tanggal_selesai}',
                                                style: darkTextFont.copyWith(
                                                  fontSize: sy(8),
                                                  fontWeight: FontWeight.w500,
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: sy(4),
                                          ),
                                        ],
                                      ),
                                    ),
                          SizedBox(
                            height: sy(10),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Row(
                                children: [
                                  Icon(
                                    Icons.circle,
                                    color: greenColor,
                                    size: 12,
                                  ),
                                  SizedBox(
                                    width: sx(8),
                                  ),
                                  Text(
                                    'Pemasukan',
                                    style: darkTextFont,
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.circle,
                                    color: errorColor,
                                    size: 12,
                                  ),
                                  SizedBox(
                                    width: sx(8),
                                  ),
                                  Text(
                                    'Pengeluaran',
                                    style: darkTextFont,
                                  )
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: sy(40),
                          ),
                          Expanded(
                            child: (_.isLoadingGetLaporanKeuanganPie.value ==
                                    true)
                                ? Center(
                                    child: SizedBox(
                                      height: sy(20),
                                      width: sy(20),
                                      child: CircularProgressIndicator(
                                        color: mainColor,
                                      ),
                                    ),
                                  )
                                : (_.isGotTimeOutLine.value == true)
                                    ? Center(
                                        child: TextButton(
                                          onPressed: () {
                                            _.isHarianPicked.value = true;
                                            _.filterStatus(0);
                                          },
                                          child: Text(
                                            'Refresh',
                                            style: mainTextFont.copyWith(
                                              fontSize: sy(10),
                                            ),
                                          ),
                                        ),
                                      )
                                    : Center(
                                        child: Visibility(
                                          visible: (_.onePercentValue.value ==
                                                  0.0)
                                              ? (_.sectionValuePemasukan
                                                              .value ==
                                                          0.0 &&
                                                      _.sectionValuePengeluaran
                                                              .value ==
                                                          0.0)
                                                  ? false
                                                  : true
                                              : true,
                                          child: PieChart(
                                            PieChartData(
                                              borderData: FlBorderData(
                                                show: false,
                                              ),
                                              sectionsSpace: 0,
                                              centerSpaceRadius: 40,
                                              sections: _.pieChartSectionData,
                                            ),
                                          ),
                                        ),
                                      ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                    TransactionView(),
                    ProfileView(),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}
