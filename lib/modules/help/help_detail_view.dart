import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:starter_1/models/bantuan.dart';
import 'package:starter_1/shared/methods.dart';
import 'package:starter_1/shared/themes.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class HelpDetailView extends StatefulWidget {
  const HelpDetailView({Key? key}) : super(key: key);

  @override
  State<HelpDetailView> createState() => _HelpDetailViewState();
}

class _HelpDetailViewState extends State<HelpDetailView> {
  late YoutubePlayerController _controller;
  late BantuanItem bantuanItem;

  @override
  void initState() {
    super.initState();

    bantuanItem = Get.arguments;

    _controller = YoutubePlayerController(
      initialVideoId: '${bantuanItem.link_v}',
    );
  }

  @override
  Widget build(BuildContext context) {
    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Detail Bantuan"),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: defaultHorizontalMargin,
                vertical: defaultVerticalMargin,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${bantuanItem.judul}',
                    style: darkTextFont.copyWith(
                      fontSize: sy(13),
                    ),
                  ),
                  SizedBox(
                    height: sy(6),
                  ),
                  YoutubePlayerIFrame(
                    controller: _controller,
                    aspectRatio: 16 / 9,
                  ),
                  SizedBox(
                    height: sy(4),
                  ),
                  Divider(
                    height: 2,
                    color: highlightColor,
                  ),
                  SizedBox(
                    height: sy(4),
                  ),
                  Text(
                    Methods.stripHtmlIfNeeded(bantuanItem.keterangan!),
                    style: blackTextFont.copyWith(
                      fontSize: sy(10),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
