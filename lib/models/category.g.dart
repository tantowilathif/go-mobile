// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Category _$CategoryFromJson(Map<String, dynamic> json) => Category(
      data: json['data'] == null
          ? null
          : CategoryList.fromJson(json['data'] as Map<String, dynamic>),
      message: json['message'] as String?,
      status: json['status'] as String?,
      status_code: json['status_code'] as int?,
    );

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'data': instance.data?.toJson(),
      'message': instance.message,
      'status': instance.status,
      'status_code': instance.status_code,
    };

CategoryList _$CategoryListFromJson(Map<String, dynamic> json) => CategoryList(
      total_items: json['total_items'] as int?,
      list: (json['list'] as List<dynamic>?)
          ?.map((e) => CategoryItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CategoryListToJson(CategoryList instance) =>
    <String, dynamic>{
      'total_items': instance.total_items,
      'list': instance.list?.map((e) => e.toJson()).toList(),
    };

CategoryItem _$CategoryItemFromJson(Map<String, dynamic> json) => CategoryItem(
      id: json['id'] as int?,
      user_id: json['user_id'] as int?,
      tipe: json['tipe'] as String?,
      nama: json['nama'] as String?,
      selected: json['selected'] as bool?,
    );

Map<String, dynamic> _$CategoryItemToJson(CategoryItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.user_id,
      'tipe': instance.tipe,
      'nama': instance.nama,
      'selected': instance.selected,
    };
