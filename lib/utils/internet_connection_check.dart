import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class InternetConnectionCheck {
  //* Singleton
  InternetConnectionCheck._internal();

  static final InternetConnectionCheck _singleton =
      InternetConnectionCheck._internal();

  static InternetConnectionCheck getInstance() => _singleton;

  //* Variables
  bool hasConnection = false;

  StreamController connectionChangeStreamController =
      StreamController.broadcast();

  final Connectivity _connectivity = Connectivity();

  //* Getter
  Stream get connectionChange => connectionChangeStreamController.stream;

  //* Methods
  void initialize() {
    _connectivity.onConnectivityChanged.listen(_connectionChanged);

    print('Listen To Internet Connectivity RUNNING!!');
  }

  void dispose() {
    connectionChangeStreamController.close();
  }

  void _connectionChanged(ConnectivityResult result) {
    checkConnectivity();
  }

  Future<bool> checkConnectivity() async {
    bool previousConnection = hasConnection;

    var connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      if (await InternetConnectionChecker().hasConnection) {
        hasConnection = true;
      } else {
        hasConnection = false;
      }
    } else {
      hasConnection = false;
    }

    if (previousConnection != hasConnection) {
      connectionChangeStreamController.add(hasConnection);
    }

    return hasConnection;
  }
}
