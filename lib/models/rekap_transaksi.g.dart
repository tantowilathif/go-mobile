// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rekap_transaksi.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RekapTransaksi _$RekapTransaksiFromJson(Map<String, dynamic> json) =>
    RekapTransaksi(
      data: json['data'] == null
          ? null
          : RekapTransaksiItem.fromJson(json['data'] as Map<String, dynamic>),
      message: json['message'] as String?,
      status: json['status'] as String?,
      status_code: json['status_code'] as int?,
    );

Map<String, dynamic> _$RekapTransaksiToJson(RekapTransaksi instance) =>
    <String, dynamic>{
      'data': instance.data?.toJson(),
      'message': instance.message,
      'status': instance.status,
      'status_code': instance.status_code,
    };

RekapTransaksiItem _$RekapTransaksiItemFromJson(Map<String, dynamic> json) =>
    RekapTransaksiItem(
      code: json['code'] as int?,
      message: json['message'] as String?,
      RekMasterTransaksi: json['RekMasterTransaksi'] == null
          ? null
          : MasterTransaksi.fromJson(
              json['RekMasterTransaksi'] as Map<String, dynamic>),
      LapdetailTransaksi: (json['LapdetailTransaksi'] as List<dynamic>?)
          ?.map((e) => ListRekapTransaksi.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$RekapTransaksiItemToJson(RekapTransaksiItem instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'RekMasterTransaksi': instance.RekMasterTransaksi?.toJson(),
      'LapdetailTransaksi':
          instance.LapdetailTransaksi?.map((e) => e.toJson()).toList(),
    };

MasterTransaksi _$MasterTransaksiFromJson(Map<String, dynamic> json) =>
    MasterTransaksi(
      pemasukan: (json['pemasukan'] as num?)?.toDouble(),
      pengeluaran: (json['pengeluaran'] as num?)?.toDouble(),
      saldo: (json['saldo'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$MasterTransaksiToJson(MasterTransaksi instance) =>
    <String, dynamic>{
      'pemasukan': instance.pemasukan,
      'pengeluaran': instance.pengeluaran,
      'saldo': instance.saldo,
    };

ListRekapTransaksi _$ListRekapTransaksiFromJson(Map<String, dynamic> json) =>
    ListRekapTransaksi(
      id: json['id'] as int?,
      tanggal: json['tanggal'] as String?,
      keterangan: json['keterangan'] as String?,
      tanggal_edit: json['tanggal_edit'] as String?,
      pengeluaran: json['pengeluaran'] as int?,
      pemasukan: json['pemasukan'] as int?,
    );

Map<String, dynamic> _$ListRekapTransaksiToJson(ListRekapTransaksi instance) =>
    <String, dynamic>{
      'id': instance.id,
      'tanggal': instance.tanggal,
      'keterangan': instance.keterangan,
      'tanggal_edit': instance.tanggal_edit,
      'pengeluaran': instance.pengeluaran,
      'pemasukan': instance.pemasukan,
    };
