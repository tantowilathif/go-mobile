import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/shared/constant.dart';

class RegisterRepository {
  //* Variables
  Dio dioService = Dio();

  Future<int?> registerRequest({required User user}) async {
    // Variables
    Response response;

    final headers = {'Content-Type': 'application/json'};

    BaseOptions options = BaseOptions(
      headers: headers,
    );

    print('AKU KIRIM INI LO == ${jsonEncode(user.toJson())}');

    try {
      response =
          await Dio(options).post("$ASYNC_API/register", data: user.toJson());

      if (response.statusCode == 200) {
        print('[SUCCES] - Register Account');
      }

      return response.statusCode;
    } on DioError catch (e) {
      print("[ERROR] - Register Account == ${e.toString()}");

      Auth auth = Auth.fromJson(e.response!.data);

      throw auth.message!;
    }
  }
}
