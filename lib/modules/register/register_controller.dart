import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart' as p;
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:starter_1/config/routes.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/modules/register/register_repository.dart';
import 'package:starter_1/utils/internet_connection_check.dart';

class RegisterController extends GetxController {
  //* Vairables for conditional view
  var isAvaEmpty = false.obs;
  var isLoadingRegister = false.obs;
  var hasConnection = true.obs;

  //* Other Variables
  final RegisterRepository registerRepository = RegisterRepository();

  final imagePicker = ImagePicker();
  Rxn<File> avatarFile = Rxn<File>();
  String? base64AvaImage;
  String? extensionImage;

  //* Cycles
  @override
  void onInit() async {
    super.onInit();

    hasConnection.value =
        await InternetConnectionCheck.getInstance().checkConnectivity();

    InternetConnectionCheck.getInstance().connectionChange.listen(
      (event) {
        connectionChanged(event);
      },
    );
  }

  //* Methods (Function)
  void connectionChanged(dynamic hasConnectionStatus) {
    hasConnection.value = hasConnectionStatus;
  }

  Future<void> chooseImageFromGallery() async {
    // Variables
    String? extension;

    final pickedFile = await imagePicker.pickImage(source: ImageSource.gallery);

    avatarFile.value = File(pickedFile!.path);

    // Convert to Base64
    extension = p.extension(pickedFile.path).trim();
    extensionImage = extension.substring(1);
    File imageFile = File(pickedFile.path);
    final imageBytes = imageFile.readAsBytesSync();
    base64AvaImage =
        "data:image/$extensionImage;base64,${base64.encode(imageBytes)}";

    update();
  }

  Future<void> register({User? textValue}) async {
    // Variables
    int? status;

    isLoadingRegister.value = true;

    User userValue = User(
      name: textValue!.name,
      email: textValue.email,
      password: textValue.password,
      file: base64AvaImage,
    );

    if (hasConnection.value == true) {
      try {
        status = await registerRepository.registerRequest(user: userValue);

        if (status == 200) {
          isLoadingRegister.value = false;

          Get.offNamed(Routes.LoginView);
        }
      } catch (e) {
        Get.snackbar('Error', 'Something wrong please try again');

        isLoadingRegister.value = false;
      }
    } else {
      Get.snackbar('Error', 'Check your internet connection');

      isLoadingRegister.value = false;
    }
  }
}
