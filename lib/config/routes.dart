// ignore_for_file: constant_identifier_names

class Routes {
  static const String SplashView = '/splash';
  static const String HomeView = '/home';
  static const String LoginView = '/login';
  static const String RegisterView = '/register';
  static const String TransactionView = '/transaction';
  static const String ProfileView = '/profile';
  static const String TransactionMasterView = '/transaction_master';
  static const String CategoryView = '/category_view';
  static const String CategoryMasterView = '/category_master';
  static const String PemasukanDetailView = '/pemasukan_detail';
  static const String PengeluaranDetailView = '/pengeluaran_detail';
  static const String ProfileMasterView = '/profile_master';
  static const String HelpView = '/help';
  static const String HelpDetailView = '/help_detail';
  static const String TransaksiUpdate = '/transaksi_update';
}
