import 'dart:convert';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/models/bantuan.dart';
import 'package:starter_1/modules/help/help_repository.dart';
import 'package:starter_1/utils/internet_connection_check.dart';

class HelpController extends GetxController {
  //* Vairables for conditional view
  var isLoadingGetBantuanList = false.obs;
  var hasConnection = true.obs;

  //* Other Variables
  final HelpRepository _helpRepository = HelpRepository();

  Bantuan? bantuanResult;
  List<BantuanItem> bantuanList = [];

  //* Cycles
  @override
  void onInit() async {
    super.onInit();

    hasConnection.value =
        await InternetConnectionCheck.getInstance().checkConnectivity();

    InternetConnectionCheck.getInstance().connectionChange.listen(
      (event) {
        connectionChanged(event);
      },
    );

    getBantuanList();
  }

  //* Methods (Function)
  void connectionChanged(dynamic hasConnectionStatus) {
    hasConnection.value = hasConnectionStatus;
  }

  Future<void> getBantuanList() async {
    // Variables
    User? userData;

    isLoadingGetBantuanList.value = true;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    print('conn == ${hasConnection.value}');

    if (hasConnection.value == true) {
      try {
        bantuanResult = await _helpRepository.listCategoryRequest(
          token: userData.token,
        );

        if (bantuanResult!.data!.list != null) {
          bantuanList = bantuanResult!.data!.list!;
        }

        isLoadingGetBantuanList.value = false;
      } catch (e) {
        print(e.toString());

        Get.snackbar('Error', 'Something wrong please try again');
      }
    } else {
      Get.snackbar('Error', 'Check your internet connection');

      isLoadingGetBantuanList.value = false;
    }

    update();
  }
}
