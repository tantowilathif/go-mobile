import 'package:get/get.dart';
import 'package:starter_1/config/routes.dart';
import 'package:starter_1/modules/category/category_master_view.dart';
import 'package:starter_1/modules/category/category_view.dart';
import 'package:starter_1/modules/help/help_detail_view.dart';
import 'package:starter_1/modules/help/help_view.dart';
import 'package:starter_1/modules/home/home_view.dart';
import 'package:starter_1/modules/login/login_view.dart';
import 'package:starter_1/modules/profile/profile_master_view.dart';
import 'package:starter_1/modules/profile/profile_view.dart';
import 'package:starter_1/modules/register/register_view.dart';
import 'package:starter_1/modules/splash/splash_view.dart';
import 'package:starter_1/modules/transaction/transaction_master_view.dart';
import 'package:starter_1/modules/transaction/transaction_pemasukan_detail_view.dart';
import 'package:starter_1/modules/transaction/transaction_pengeluaran_detail_view.dart';
import 'package:starter_1/modules/transaction/transaction_update_view.dart';
import 'package:starter_1/modules/transaction/transaction_view.dart';

class PageRoutes {
  static List<GetPage> pages() {
    return [
      GetPage(
        name: Routes.SplashView,
        page: () => SplashView(),
      ),
      GetPage(
        name: Routes.LoginView,
        page: () => LoginView(),
      ),
      GetPage(
        name: Routes.RegisterView,
        page: () => RegisterView(),
      ),
      GetPage(
        name: Routes.HomeView,
        page: () => HomeView(),
      ),
      GetPage(
        name: Routes.TransactionView,
        page: () => TransactionView(),
      ),
      GetPage(
        name: Routes.ProfileView,
        page: () => ProfileView(),
      ),
      GetPage(
        name: Routes.TransactionMasterView,
        page: () => TransactionMasterView(),
      ),
      GetPage(
        name: Routes.CategoryView,
        page: () => CategoryView(),
      ),
      GetPage(
        name: Routes.CategoryMasterView,
        page: () => CategoryMasterView(),
      ),
      GetPage(
        name: Routes.PemasukanDetailView,
        page: () => TransactionPemasukanDetailView(),
      ),
      GetPage(
        name: Routes.PengeluaranDetailView,
        page: () => TransactionPengeluaranDetail(),
      ),
      GetPage(
        name: Routes.ProfileMasterView,
        page: () => ProfileMasterView(),
      ),
      GetPage(
        name: Routes.HelpView,
        page: () => HelpView(),
      ),
      GetPage(
        name: Routes.HelpDetailView,
        page: () => HelpDetailView(),
      ),
      GetPage(
        name: Routes.TransaksiUpdate,
        page: () => TransactionUpdateView(),
      ),
    ];
  }
}
