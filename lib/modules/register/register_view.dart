// ignore_for_file: must_be_immutable, prefer_final_fields

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/modules/register/register_controller.dart';
import 'package:starter_1/shared/methods.dart';
import 'package:starter_1/shared/themes.dart';
import 'package:validators/validators.dart';

class RegisterView extends StatelessWidget {
  RegisterView({Key? key}) : super(key: key);

  //* Text Editing Controllers
  TextEditingController _txtNameController = TextEditingController();
  TextEditingController _txtEmailUsernameController = TextEditingController();
  TextEditingController _txtPasswordController = TextEditingController();
  TextEditingController _txtConfirmPasswordController = TextEditingController();

  //* Text Field Focus Nodes
  final _txtNameFocusNode = FocusNode();
  final _txtEmailUsernameFocusNode = FocusNode();
  final _txtPasswordFocusNode = FocusNode();
  final _txtConfirmPasswordFocusNode = FocusNode();

  //* Variables
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    RegisterController _registerController = Get.put(RegisterController());

    //* Methods (Function)
    _pickImage() {
      _registerController.chooseImageFromGallery();
    }

    _onClickRegister() {
      if (_registerController.avatarFile.value == null) {
        if (_formKey.currentState!.validate()) {
          _formKey.currentState!.save();
        }
        _registerController.isAvaEmpty.value = true;
      } else {
        _registerController.isAvaEmpty.value = false;

        if (_formKey.currentState!.validate()) {
          _formKey.currentState!.save();

          User user = User(
            name: _txtNameController.text,
            email: _txtEmailUsernameController.text.trim(),
            password: _txtConfirmPasswordController.text,
          );

          _registerController.register(textValue: user);
        }
      }
    }

    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return GetX<RegisterController>(
            initState: (_) {},
            builder: (_) {
              return Scaffold(
                resizeToAvoidBottomInset: false,
                appBar: AppBar(
                  centerTitle: true,
                  title: Text('Register'),
                ),
                body: InkWell(
                  onTap: () {
                    FocusScope.of(context).unfocus();
                  },
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  focusColor: Colors.transparent,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: defaultHorizontalMargin,
                      vertical: defaultVerticalMargin,
                    ),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Spacer(),
                          Stack(
                            alignment: Alignment.bottomCenter,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                child: Container(
                                  width: sy(80),
                                  height: sy(80),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: greyColor.withOpacity(0.5),
                                    ),
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                      image: (_.avatarFile.value != null)
                                          ? FileImage(_.avatarFile.value!)
                                          : AssetImage(
                                              "assets/images/default_avatar.png",
                                            ) as ImageProvider,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 200,
                                top: 80,
                                child: Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: mainColor,
                                  ),
                                  height: sy(25),
                                  child: IconButton(
                                    splashRadius: 24,
                                    padding: EdgeInsets.all(2),
                                    onPressed: () {
                                      _pickImage();
                                    },
                                    icon: Icon(Icons.camera_alt_rounded),
                                    color: whiteColor,
                                    iconSize: 20,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: sy(10),
                          ),
                          Visibility(
                            visible: _.isAvaEmpty.value,
                            child: Text(
                              "Tambahkan Avatar",
                              style: errorTextFont.copyWith(
                                fontSize: sy(9),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: sy(10),
                          ),
                          TextFormField(
                            controller: _txtNameController,
                            focusNode: _txtNameFocusNode,
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (value) {
                              Methods.txtFieldFocusChange(
                                context,
                                _txtNameFocusNode,
                                _txtEmailUsernameFocusNode,
                              );
                            },
                            validator: (value) {
                              if (isNull(value)) {
                                return 'Nama harus di isi';
                              } else if (value!.length <= 1) {
                                return 'Nama Invalid ';
                              }
                            },
                            decoration: InputDecoration(
                              alignLabelWithHint: true,
                              labelText: 'Nama',
                              hintText: 'Masukkan Nama anda',
                              isDense: true,
                              errorStyle: errorTextFont.copyWith(
                                fontSize: sy(8),
                                height: 0.6,
                              ),
                              enabledBorder: enabledBorder,
                              focusedBorder: focusedBorder,
                              errorBorder: errorBorder,
                              focusedErrorBorder: focusedError,
                              labelStyle: secondBlackTextFont,
                              floatingLabelStyle: secondBlackTextFont,
                              hintStyle:
                                  greyTextFont.copyWith(fontSize: sy(10)),
                            ),
                          ),
                          TextFormField(
                            controller: _txtEmailUsernameController,
                            focusNode: _txtEmailUsernameFocusNode,
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (value) {
                              Methods.txtFieldFocusChange(
                                context,
                                _txtEmailUsernameFocusNode,
                                _txtPasswordFocusNode,
                              );
                            },
                            validator: (value) {
                              if (isNull(value)) {
                                return 'Email harus di isi';
                              } else if (!isEmail(value!)) {
                                return 'Email Invalid ';
                              }
                            },
                            decoration: InputDecoration(
                              alignLabelWithHint: true,
                              labelText: 'Email',
                              hintText: 'Masukkan Email anda',
                              isDense: true,
                              errorStyle: errorTextFont.copyWith(
                                fontSize: sy(8),
                                height: 0.6,
                              ),
                              enabledBorder: enabledBorder,
                              focusedBorder: focusedBorder,
                              errorBorder: errorBorder,
                              focusedErrorBorder: focusedError,
                              labelStyle: secondBlackTextFont,
                              floatingLabelStyle: secondBlackTextFont,
                              hintStyle:
                                  greyTextFont.copyWith(fontSize: sy(10)),
                            ),
                          ),
                          TextFormField(
                            obscureText: true,
                            controller: _txtPasswordController,
                            focusNode: _txtPasswordFocusNode,
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (value) {
                              Methods.txtFieldFocusChange(
                                context,
                                _txtPasswordFocusNode,
                                _txtConfirmPasswordFocusNode,
                              );
                            },
                            validator: (value) {
                              if (isNull(value)) {
                                return 'Password harus di isi';
                              } else if (value!.length < 6) {
                                return 'Password invalid minimal 6 karakter';
                              }
                            },
                            decoration: InputDecoration(
                              alignLabelWithHint: true,
                              labelText: 'Password',
                              hintText: 'Masukkan Password anda',
                              isDense: true,
                              errorStyle: errorTextFont.copyWith(
                                fontSize: sy(8),
                                height: 0.6,
                              ),
                              enabledBorder: enabledBorder,
                              focusedBorder: focusedBorder,
                              errorBorder: errorBorder,
                              focusedErrorBorder: focusedError,
                              labelStyle: secondBlackTextFont,
                              floatingLabelStyle: secondBlackTextFont,
                              hintStyle:
                                  greyTextFont.copyWith(fontSize: sy(10)),
                            ),
                          ),
                          TextFormField(
                            obscureText: true,
                            controller: _txtConfirmPasswordController,
                            focusNode: _txtConfirmPasswordFocusNode,
                            textInputAction: TextInputAction.next,
                            onFieldSubmitted: (value) {
                              FocusScope.of(context).unfocus();
                            },
                            validator: (value) {
                              if (isNull(value)) {
                                return 'Password harus di isi';
                              } else if (value!.length < 6) {
                                return 'Password invalid minimal 6 karakter';
                              } else if (value != _txtPasswordController.text) {
                                return 'Password tidak cocok';
                              }
                            },
                            decoration: InputDecoration(
                              alignLabelWithHint: true,
                              labelText: 'Konfirmasi Password',
                              hintText: 'Masukkan kembali password anda',
                              isDense: true,
                              errorStyle: errorTextFont.copyWith(
                                fontSize: sy(8),
                                height: 0.6,
                              ),
                              enabledBorder: enabledBorder,
                              focusedBorder: focusedBorder,
                              errorBorder: errorBorder,
                              focusedErrorBorder: focusedError,
                              labelStyle: secondBlackTextFont,
                              floatingLabelStyle: secondBlackTextFont,
                              hintStyle:
                                  greyTextFont.copyWith(fontSize: sy(10)),
                            ),
                          ),
                          SizedBox(
                            height: sy(40),
                          ),
                          Obx(() => TextButton(
                                style: TextButton.styleFrom(
                                  alignment: Alignment.center,
                                  backgroundColor: mainColor,
                                  elevation: 0,
                                  fixedSize: Size(200, double.infinity),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    side: BorderSide(color: Colors.transparent),
                                  ),
                                  primary: whiteColor,
                                ),
                                child: (_.isLoadingRegister.value == false)
                                    ? Text(
                                        "Register",
                                        style: whiteTextFont.copyWith(
                                            fontSize: sy(15)),
                                      )
                                    : SizedBox(
                                        height: sy(20),
                                        width: sy(20),
                                        child: CircularProgressIndicator(
                                          color: whiteColor,
                                        ),
                                      ),
                                onPressed: () {
                                  _onClickRegister();
                                },
                              )),
                          Spacer(),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            });
      },
    );
  }
}
