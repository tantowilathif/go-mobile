import 'package:json_annotation/json_annotation.dart';

part 'transaksi.g.dart';

// ignore_for_file: non_constant_identifier_names

@JsonSerializable(explicitToJson: true)
class Transaksi {
  int? id;
  String? tipe;
  double? jumlah_masuk;
  double? jumlah_keluar;
  String? tanggal;
  String? keterangan;
  int? kategori_id;
  int? user_id;

  Transaksi({
    this.id,
    this.tipe,
    this.jumlah_masuk,
    this.jumlah_keluar,
    this.tanggal,
    this.keterangan,
    this.kategori_id,
    this.user_id,
  });

  factory Transaksi.fromJson(Map<String, dynamic> parsedJson) =>
      _$TransaksiFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$TransaksiToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Transaksi2 {
  int? id;
  String? tipe;
  double? jumlah_masuk;
  double? jumlah_keluar;
  String? tanggal;
  String? tanggal_edit;
  String? keterangan;
  int? kategori_id;
  int? user_id;

  Transaksi2({
    this.id,
    this.tipe,
    this.jumlah_masuk,
    this.jumlah_keluar,
    this.tanggal,
    this.tanggal_edit,
    this.keterangan,
    this.kategori_id,
    this.user_id,
  });

  factory Transaksi2.fromJson(Map<String, dynamic> parsedJson) =>
      _$Transaksi2FromJson(parsedJson);

  Map<String, dynamic> toJson() => _$Transaksi2ToJson(this);
}
