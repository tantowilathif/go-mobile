import 'dart:convert';
import 'dart:io';

import 'package:get/get.dart';
import 'package:path/path.dart' as p;
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/modules/profile/profile_repository.dart';
import 'package:starter_1/utils/internet_connection_check.dart';

class ProfileController extends GetxController {
  //* Vairables for conditional view
  var isLoadingUpdateProfile = false.obs;
  var hasConnection = true.obs;

  //* Other Variables
  final ProfileRepository _profileRepository = ProfileRepository();

  Auth? authResults;
  User? userData;
  User? userArgs;
  String? nama = '...';
  String? email = '...';
  String? urlAvatar = '...';

  final imagePicker = ImagePicker();
  Rxn<File> avatarFile = Rxn<File>();
  String? base64AvaImage;
  String? extensionImage;

  //* Cycles
  @override
  void onInit() async {
    super.onInit();

    hasConnection.value =
        await InternetConnectionCheck.getInstance().checkConnectivity();

    InternetConnectionCheck.getInstance().connectionChange.listen(
      (event) {
        connectionChanged(event);
      },
    );
  }

  //* Methods (Function)
  void connectionChanged(dynamic hasConnectionStatus) {
    hasConnection.value = hasConnectionStatus;
  }

  void getUserData() async {
    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    nama = userData!.name;
    email = userData!.email;
    urlAvatar = userData!.url;

    print('AVATAR URL == $urlAvatar');

    update();
  }

  Future<void> chooseNewImageFromGallery() async {
    // Variables
    String? extension;

    final pickedFile = await imagePicker.pickImage(source: ImageSource.gallery);

    avatarFile.value = File(pickedFile!.path);

    // Convert to Base64
    extension = p.extension(pickedFile.path).trim();
    extensionImage = extension.substring(1);
    File imageFile = File(pickedFile.path);
    final imageBytes = imageFile.readAsBytesSync();
    base64AvaImage =
        "data:image/$extensionImage;base64,${base64.encode(imageBytes)}";

    update();
  }

  Future<void> updateProfile({User? textValue, int? id}) async {
    late User userValue;

    isLoadingUpdateProfile.value = true;

    if (avatarFile.value == null) {
      userValue = User(
        id: id,
        name: textValue!.name,
        email: textValue.email,
        username: textValue.username,
      );
    } else {
      userValue = User(
        id: id,
        name: textValue!.name,
        email: textValue.email,
        username: textValue.username,
        file: base64AvaImage,
      );
    }

    if (hasConnection.value == true) {
      try {
        // Init shared preference
        SharedPreferences prefs = await SharedPreferences.getInstance();

        authResults = await _profileRepository.updateRequest(user: userValue);

        await prefs.setString(
          'user',
          jsonEncode(
            authResults!.data!.toJson(),
          ),
        );

        print('${jsonDecode(prefs.getString('user')!)}');

        isLoadingUpdateProfile.value = false;

        getUserData();

        Get.back();
      } catch (e) {
        isLoadingUpdateProfile.value = false;

        if (e.toString() == "Connection Timeout Coba Lagi") {
          Get.snackbar('Timeout', 'Silahkan Coba Lagi');
        } else {
          Get.snackbar('Error', 'Something wrong please try again');
        }
      }
    } else {
      Get.snackbar('Error', 'Check your internet connection');

      isLoadingUpdateProfile.value = false;
    }
  }
}
