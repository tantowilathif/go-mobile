import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:starter_1/config/routes.dart';

class SplashView extends StatefulWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    super.initState();

    Future.delayed(
      Duration(seconds: 2),
      () async {
        // Init shared preference
        SharedPreferences prefs = await SharedPreferences.getInstance();

        if (prefs.getString('user') == null) {
          Get.offAllNamed(Routes.LoginView);
        } else {
          Get.offAllNamed(Routes.HomeView);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return Scaffold(
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/images/qoins_logo2.png",
                  height: sy(180),
                  width: sx(240),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
