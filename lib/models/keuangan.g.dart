// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'keuangan.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Keuangan _$KeuanganFromJson(Map<String, dynamic> json) => Keuangan(
      data: json['data'] == null
          ? null
          : KeuanganItem.fromJson(json['data'] as Map<String, dynamic>),
      message: json['message'] as String?,
      status: json['status'] as String?,
      status_code: json['status_code'] as int?,
    );

Map<String, dynamic> _$KeuanganToJson(Keuangan instance) => <String, dynamic>{
      'data': instance.data?.toJson(),
      'message': instance.message,
      'status': instance.status,
      'status_code': instance.status_code,
    };

KeuanganItem _$KeuanganItemFromJson(Map<String, dynamic> json) => KeuanganItem(
      tipe: json['tipe'] as String?,
      tanggal_mulai: json['tanggal_mulai'] as String?,
      tanggal_selesai: json['tanggal_selesai'] as String?,
      pemasukan: (json['pemasukan'] as List<dynamic>?)
          ?.map((e) => (e as num).toDouble())
          .toList(),
      pengeluaran: (json['pengeluaran'] as List<dynamic>?)
          ?.map((e) => (e as num).toDouble())
          .toList(),
      label:
          (json['label'] as List<dynamic>?)?.map((e) => e as String).toList(),
      total_items: json['total_items'] as int?,
    );

Map<String, dynamic> _$KeuanganItemToJson(KeuanganItem instance) =>
    <String, dynamic>{
      'tipe': instance.tipe,
      'tanggal_mulai': instance.tanggal_mulai,
      'tanggal_selesai': instance.tanggal_selesai,
      'pemasukan': instance.pemasukan,
      'pengeluaran': instance.pengeluaran,
      'label': instance.label,
      'total_items': instance.total_items,
    };
