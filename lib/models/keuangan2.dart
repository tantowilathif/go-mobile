import 'package:json_annotation/json_annotation.dart';

part 'keuangan2.g.dart';

// ignore_for_file: non_constant_identifier_names

@JsonSerializable(explicitToJson: true)
class Keuangan2 {
  KeuanganItem2? data;
  String? message;
  String? status;
  int? status_code;

  Keuangan2({
    this.data,
    this.message,
    this.status,
    this.status_code,
  });

  factory Keuangan2.fromJson(Map<String, dynamic> parsedJson) =>
      _$Keuangan2FromJson(parsedJson);

  Map<String, dynamic> toJson() => _$Keuangan2ToJson(this);
}

@JsonSerializable(explicitToJson: true)
class KeuanganItem2 {
  double? pemasukan;
  double? pengeluaran;
  double? saldo;
  double? total;

  KeuanganItem2({
    this.pemasukan,
    this.pengeluaran,
    this.saldo,
    this.total,
  });

  factory KeuanganItem2.fromJson(Map<String, dynamic> parsedJson) =>
      _$KeuanganItem2FromJson(parsedJson);

  Map<String, dynamic> toJson() => _$KeuanganItem2ToJson(this);
}
