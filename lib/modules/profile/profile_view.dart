import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:starter_1/config/routes.dart';
import 'package:starter_1/modules/profile/profile_controller.dart';
import 'package:starter_1/shared/themes.dart';

class ProfileView extends StatelessWidget {
  const ProfileView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ProfileController _profileController = Get.put(ProfileController());

    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return GetBuilder<ProfileController>(
          initState: (_) {
            _profileController.getUserData();
          },
          builder: (_) {
            return Scaffold(
              body: SafeArea(
                child: Container(
                  padding: EdgeInsets.symmetric(
                    vertical: defaultVerticalMargin,
                    horizontal: defaultHorizontalMargin,
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: sy(5),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: Container(
                          width: sy(70),
                          height: sy(70),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: NetworkImage("http://${_.urlAvatar}"),
                                fit: BoxFit.cover),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: sy(10),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Get.toNamed(
                                Routes.ProfileMasterView,
                                arguments: _.userData,
                              );
                            },
                            child: Text(
                              'Ubah Profile',
                              style: mainTextFont.copyWith(fontSize: sy(11)),
                            ),
                          ),
                          Text(
                            'Bergabung sejak 28-10-2021',
                            style: greyTextFont.copyWith(fontSize: sy(10)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: sy(15),
                      ),
                      Material(
                        elevation: 4,
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        child: Column(
                          children: [
                            ListTile(
                              leading: Icon(
                                Icons.person_rounded,
                                size: 28,
                              ),
                              title: Text(
                                'Nama',
                                style: highlightTextFont.copyWith(
                                  fontSize: sy(10),
                                ),
                              ),
                              subtitle: Text(
                                _.nama ?? "...",
                                style: darkTextFont.copyWith(
                                  fontSize: sy(12),
                                ),
                              ),
                            ),
                            ListTile(
                              leading: Icon(
                                Icons.email_rounded,
                                size: 28,
                              ),
                              title: Text(
                                'Email',
                                style: highlightTextFont.copyWith(
                                  fontSize: sy(10),
                                ),
                              ),
                              subtitle: Text(
                                _.email ?? "...",
                                style: darkTextFont.copyWith(
                                  fontSize: sy(12),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: sy(10),
                      ),
                      GestureDetector(
                        onTap: () {
                          Get.toNamed(Routes.HelpView);
                        },
                        child: ListTile(
                          leading: Icon(
                            Icons.help_center_rounded,
                            size: 26,
                          ),
                          title: Text(
                            'Bantuan & FAQ',
                            style: highlightTextFont.copyWith(
                              fontSize: sy(12),
                            ),
                          ),
                        ),
                      ),
                      Spacer(),
                      Container(
                        padding: EdgeInsets.symmetric(
                          vertical: defaultVerticalMargin,
                          horizontal: defaultHorizontalMargin,
                        ),
                        height: sy(45),
                        width: double.infinity,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            alignment: Alignment.center,
                            backgroundColor: mainColor,
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            primary: whiteColor,
                          ),
                          child: Text(
                            "Keluar",
                            style: whiteTextFont.copyWith(
                              fontSize: sy(12),
                            ),
                          ),
                          onPressed: () async {
                            // Init shared preferences
                            SharedPreferences preferences =
                                await SharedPreferences.getInstance();

                            preferences.clear();

                            Get.offAllNamed(Routes.LoginView);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
