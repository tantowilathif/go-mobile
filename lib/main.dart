import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:starter_1/config/notification_config.dart';
import 'package:starter_1/config/routes.dart';
import 'package:starter_1/shared/themes.dart';
import 'package:starter_1/utils/internet_connection_check.dart';

import 'config/page_routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  //* Change Statur Bar Color
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ),
  );

  //* Force orientation screen always portrait
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  //* Init firebase database
  await Firebase.initializeApp();

  //* Init Firebase Notification
  NotificationConfig.init();

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();

    InternetConnectionCheck.getInstance().initialize();
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Qoins',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: whiteColor,
        primaryColor: Colors.white,
        appBarTheme: AppBarTheme(
          color: mainDarkColor2,
          elevation: 0,
        ),
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: Routes.SplashView,
      getPages: PageRoutes.pages(),
    );
  }
}
