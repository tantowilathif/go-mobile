import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_masked_text2/flutter_masked_text2.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/models/category.dart';
import 'package:starter_1/models/transaksi.dart';
import 'package:starter_1/modules/transaction/transaction_controller.dart';
import 'package:starter_1/modules/transaction/transaction_master_repository.dart';
import 'package:starter_1/utils/internet_connection_check.dart';

class TransactionMasterController extends GetxController
    with GetSingleTickerProviderStateMixin {
  //* Controller
  TabController? tabController;

  //* Variable for conditional view
  var isLoadingCreateTransaksiPengeluaran = false.obs;
  var isLoadingCreateTransaksiPemasukan = false.obs;
  var hasConnection = true.obs;

  //* Text Editing Controllers
  var txtNominalController = MoneyMaskedTextController(
      precision: 0, decimalSeparator: "", initialValue: 0);

  TextEditingController txtNamaController = TextEditingController();
  Rx<TextEditingController> txtKategoriController = TextEditingController().obs;
  Rx<TextEditingController> txtTanggalController = TextEditingController().obs;

  var txtNominalController2 = MoneyMaskedTextController(
      precision: 0, decimalSeparator: "", initialValue: 0);
  TextEditingController txtNamaController2 = TextEditingController();
  Rx<TextEditingController> txtKategoriController2 =
      TextEditingController().obs;
  Rx<TextEditingController> txtTanggalController2 = TextEditingController().obs;

  //* Text Field Focus Nodes
  final txtNominalFocusNode = FocusNode();
  final txtNamaFocusNode = FocusNode();
  final txtNominalFocusNode2 = FocusNode();
  final txtNamaFocusNode2 = FocusNode();

  //* Other Variables
  final TransactionMasterRepository _transactionMasterRepository =
      TransactionMasterRepository();

  CategoryItem? categoryItemMasuk;
  CategoryItem? categoryItemKeluar;

  //* Cycles
  @override
  void onInit() async {
    super.onInit();

    tabController = TabController(vsync: this, length: 2);

    hasConnection.value =
        await InternetConnectionCheck.getInstance().checkConnectivity();

    InternetConnectionCheck.getInstance().connectionChange.listen(
      (event) {
        connectionChanged(event);
      },
    );
  }

  //* Methods (Function)
  void connectionChanged(dynamic hasConnectionStatus) {
    hasConnection.value = hasConnectionStatus;
  }

  Future<void> createTransaksiKeluar(
      {double? nominalKeluar, String? nama, String? tanggalValue}) async {
    // Variables
    User? userData;
    Transaksi? transaksiValue;

    isLoadingCreateTransaksiPengeluaran.value = true;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    if (tanggalValue == "") {
      tanggalValue = DateFormat('yyyy-MM-dd').format(DateTime.now());
    }

    if (hasConnection.value == true) {
      try {
        if (categoryItemKeluar == null) {
          transaksiValue = Transaksi(
              tipe: 'pengeluaran',
              jumlah_keluar: nominalKeluar,
              jumlah_masuk: 0,
              kategori_id: null,
              user_id: userData.id,
              keterangan: nama,
              tanggal: tanggalValue);
        } else {
          transaksiValue = Transaksi(
            tipe: 'pengeluaran',
            jumlah_keluar: nominalKeluar,
            jumlah_masuk: 0,
            kategori_id: categoryItemKeluar!.id,
            user_id: userData.id,
            keterangan: nama,
            tanggal: tanggalValue,
          );
        }

        await _transactionMasterRepository.createTransaksiRequest(
          token: userData.token,
          transaksiItem: transaksiValue,
        );

        Get.find<TransactionController>().filterStatus(0);

        Get.back();

        isLoadingCreateTransaksiPengeluaran.value = false;
      } catch (e) {
        isLoadingCreateTransaksiPengeluaran.value = false;

        if (e.toString() == "Connection Timeout Coba Lagi") {
          Get.snackbar('Timeout', 'Silahkan Coba Lagi');
        } else {
          Get.snackbar('Error', 'Something wrong please try again');
        }
      }
    } else {
      isLoadingCreateTransaksiPengeluaran.value = false;

      Get.snackbar('Error', 'Check your internet connection');
    }
  }

  Future<void> createTransaksiMasuk(
      {double? nominalMasuk, String? nama, String? tanggalValue}) async {
    // Variables
    User? userData;
    Transaksi? transaksiValue;

    isLoadingCreateTransaksiPemasukan.value = true;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    if (tanggalValue == "") {
      tanggalValue = DateFormat('yyyy-MM-dd').format(DateTime.now());
    }

    if (hasConnection.value == true) {
      try {
        if (categoryItemMasuk == null) {
          transaksiValue = Transaksi(
            tipe: 'pemasukan',
            jumlah_keluar: 0,
            jumlah_masuk: nominalMasuk,
            kategori_id: null,
            user_id: userData.id,
            keterangan: nama,
            tanggal: tanggalValue,
          );
        } else {
          transaksiValue = Transaksi(
            tipe: 'pemasukan',
            jumlah_keluar: 0,
            jumlah_masuk: nominalMasuk,
            kategori_id: categoryItemMasuk!.id,
            user_id: userData.id,
            keterangan: nama,
            tanggal: tanggalValue,
          );
        }

        await _transactionMasterRepository.createTransaksiRequest(
          token: userData.token,
          transaksiItem: transaksiValue,
        );

        Get.find<TransactionController>().filterStatus(0);

        Get.back();

        isLoadingCreateTransaksiPemasukan.value = false;
      } catch (e) {
        isLoadingCreateTransaksiPemasukan.value = false;

        if (e.toString() == "Connection Timeout Coba Lagi") {
          Get.snackbar('Timeout', 'Silahkan Coba Lagi');
        } else {
          Get.snackbar('Error', 'Something wrong please try again');
        }
      }
    } else {
      isLoadingCreateTransaksiPemasukan.value = false;

      Get.snackbar('Error', 'Check your internet connection');
    }
  }
}
