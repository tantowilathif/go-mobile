import 'package:dio/dio.dart';
import 'package:starter_1/models/rekap_transaksi.dart';
import 'package:starter_1/models/transaksi.dart';
import 'package:starter_1/shared/constant.dart';

class TransactionRepository {
  Future<RekapTransaksi?> rekapTransaksiRequest({
    int? userId,
    String? token,
    String? tanggalMulai,
    String? tanggalSelesai,
  }) async {
    // Variables
    Response response;

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };

    BaseOptions options = BaseOptions(
      headers: headers,
      connectTimeout: 10000,
    );

    try {
      response = await Dio(options).get(
        "$API/laptransaksi",
        queryParameters: {
          "user_id": "$userId",
          "tanggal_mulai": "$tanggalMulai",
          "tanggal_selesai": "$tanggalSelesai",
        },
      );

      print('[SUCCES] - Get Rekap Transaksi');

      return RekapTransaksi.fromJson(response.data);
    } on DioError catch (e) {
      print("[ERROR] - Get Rekap Transaksi == ${e.toString()}");
      if (e.type == DioErrorType.connectTimeout) {
        throw "Connection Timeout Coba Lagi";
      } else {
        RekapTransaksi rekapTransaksi =
            RekapTransaksi.fromJson(e.response!.data);

        throw rekapTransaksi.message!;
      }
    }
  }

  Future<void> updateTransaksi({
    int? userId,
    String? token,
    Transaksi? value,
  }) async {
    // Variables
    Response response;

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };

    BaseOptions options = BaseOptions(
      headers: headers,
      connectTimeout: 10000,
    );

    try {
      response = await Dio(options).put(
        "$API/transaksi",
        data: value!.toJson(),
      );

      print('[SUCCES - ${response.statusCode}] - Update Transaksi');
    } on DioError catch (e) {
      if (e.type == DioErrorType.connectTimeout) {
        throw "Connection Timeout Coba Lagi";
      } else {
        print("[ERROR] - Delete Transaksi == ${e.toString()}");

        throw e.toString();
      }
    }
  }

  Future<void> deleteTransaksi({
    String? token,
    int? valueId,
  }) async {
    // Variables
    Response response;

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };

    BaseOptions options = BaseOptions(
      headers: headers,
    );

    try {
      response = await Dio(options).delete(
        "$API/transaksi/$valueId",
      );

      print('[SUCCES - ${response.statusCode}] - Delete Transaksi');
    } on DioError catch (e) {
      print("[ERROR] - Delete Transaksi == ${e.toString()}");

      throw e.toString();
    }
  }
}
