// ignore_for_file: must_be_immutable, prefer_final_fields

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:starter_1/config/routes.dart';
import 'package:starter_1/modules/transaction/transaction_master_controller.dart';
import 'package:starter_1/shared/methods.dart';
import 'package:starter_1/shared/themes.dart';
import 'package:validators/validators.dart';

class TransactionMasterView extends StatelessWidget {
  TransactionMasterView({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    TransactionMasterController transactionMasterController =
        Get.put(TransactionMasterController());

    transactionMasterController.tabController!.animation!.addListener(
      () {
        FocusScope.of(context).unfocus();
      },
    );

    _onClickTambahPengeluaran() {
      if (_formKey.currentState!.validate()) {
        _formKey.currentState!.save();

        transactionMasterController.createTransaksiKeluar(
          nama: transactionMasterController.txtNamaController.value.text,
          nominalKeluar: double.parse(
            transactionMasterController.txtNominalController.text
                .split('.')
                .join(""),
          ),
          tanggalValue:
              transactionMasterController.txtTanggalController.value.text,
        );
      }
    }

    _onClickTambahPemasukan() {
      if (_formKey.currentState!.validate()) {
        _formKey.currentState!.save();

        transactionMasterController.createTransaksiMasuk(
          nama: transactionMasterController.txtNamaController2.value.text,
          nominalMasuk: double.parse(
            transactionMasterController.txtNominalController2.text
                .split('.')
                .join(""),
          ),
          tanggalValue:
              transactionMasterController.txtTanggalController2.value.text,
        );
      }
    }

    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return GetX<TransactionMasterController>(initState: (_) {
          transactionMasterController.txtTanggalController.value.text =
              DateFormat('yyyy-MM-dd').format(DateTime.now());
          transactionMasterController.txtTanggalController2.value.text =
              DateFormat('yyyy-MM-dd').format(DateTime.now());
        }, builder: (_) {
          return Scaffold(
            resizeToAvoidBottomInset: false,
            body: SafeArea(
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    TabBar(
                      unselectedLabelColor: highlightColor,
                      labelColor: mainColor,
                      indicatorColor: mainColor,
                      tabs: [
                        Tab(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.account_balance_wallet_rounded,
                                color: errorColor,
                              ),
                              SizedBox(
                                width: sx(8),
                              ),
                              Text(
                                'Pengeluaran',
                                style: errorTextFont.copyWith(
                                  fontSize: sy(10),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          iconMargin: EdgeInsets.all(2),
                        ),
                        Tab(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.attach_money_rounded,
                                color: greenColor,
                              ),
                              SizedBox(
                                width: sx(8),
                              ),
                              Text(
                                'Pemasukan',
                                style: greenTextFont.copyWith(
                                  fontSize: sy(10),
                                ),
                              ),
                            ],
                          ),
                          iconMargin: EdgeInsets.all(2),
                        ),
                      ],
                      controller: transactionMasterController.tabController,
                      indicatorSize: TabBarIndicatorSize.tab,
                      onTap: (index) {
                        FocusScope.of(context).unfocus();
                      },
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          FocusScope.of(context).unfocus();
                        },
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        focusColor: Colors.transparent,
                        child: Container(
                          padding: EdgeInsets.symmetric(
                            vertical: defaultVerticalMargin,
                            horizontal: defaultHorizontalMargin,
                          ),
                          // color: whiteColor.withOpacity(1),
                          child: TabBarView(
                            controller:
                                transactionMasterController.tabController,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Material(
                                    elevation: 4,
                                    borderRadius: BorderRadius.circular(8),
                                    color: whiteColor,
                                    child: Container(
                                      padding: EdgeInsets.only(
                                        left: 20,
                                        right: 10,
                                        top: 10,
                                        bottom: 30,
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            keyboardType: TextInputType.number,
                                            controller: _.txtNominalController,
                                            focusNode: _.txtNominalFocusNode,
                                            textInputAction:
                                                TextInputAction.next,
                                            onFieldSubmitted: (value) {
                                              Methods.txtFieldFocusChange(
                                                context,
                                                _.txtNominalFocusNode,
                                                _.txtNamaFocusNode,
                                              );
                                            },
                                            validator: (value) {
                                              if (isNull(value)) {
                                                return 'Nominal harus di isi';
                                              } else if (double.parse(
                                                      transactionMasterController
                                                          .txtNominalController
                                                          .text
                                                          .split('.')
                                                          .join("")) ==
                                                  0) {
                                                return 'Nominal harus di isi';
                                              } else if (double.parse(
                                                      transactionMasterController
                                                          .txtNominalController
                                                          .text
                                                          .split('.')
                                                          .join("")) >
                                                  999999) {
                                                return 'Maksimal Nominal Rp 999.999';
                                              } else if (double.parse(
                                                      transactionMasterController
                                                          .txtNominalController
                                                          .text
                                                          .split('.')
                                                          .join("")) <
                                                  100) {
                                                return 'Minimal Nominal Rp 100';
                                              }
                                            },
                                            decoration: InputDecoration(
                                              alignLabelWithHint: true,
                                              labelText: 'Nominal',
                                              hintText: 'Masukkan nominal',
                                              isDense: true,
                                              errorStyle:
                                                  errorTextFont.copyWith(
                                                fontSize: sy(8),
                                                height: 0.6,
                                              ),
                                              enabledBorder: enabledBorder,
                                              focusedBorder: focusedBorder,
                                              errorBorder: errorBorder,
                                              focusedErrorBorder: focusedError,
                                              labelStyle: secondBlackTextFont,
                                              floatingLabelStyle:
                                                  secondBlackTextFont,
                                              hintStyle: greyTextFont.copyWith(
                                                  fontSize: sy(10)),
                                            ),
                                          ),
                                          TextFormField(
                                            controller: _.txtNamaController,
                                            focusNode: _.txtNamaFocusNode,
                                            textInputAction:
                                                TextInputAction.done,
                                            onFieldSubmitted: (value) {
                                              FocusScope.of(context).unfocus();
                                            },
                                            validator: (value) {},
                                            decoration: InputDecoration(
                                              alignLabelWithHint: true,
                                              labelText: 'Nama Pengeluaran',
                                              hintText:
                                                  'Masukkan nama pengeluaran',
                                              isDense: true,
                                              enabledBorder: enabledBorder,
                                              focusedBorder: focusedBorder,
                                              labelStyle: secondBlackTextFont,
                                              floatingLabelStyle:
                                                  secondBlackTextFont,
                                              hintStyle: greyTextFont.copyWith(
                                                  fontSize: sy(10)),
                                            ),
                                          ),
                                          SizedBox(
                                            height: sy(5),
                                          ),
                                          TextFormField(
                                            enableInteractiveSelection: false,
                                            readOnly: true,
                                            controller:
                                                _.txtKategoriController.value,
                                            onFieldSubmitted: (value) {},
                                            validator: (value) {},
                                            decoration: InputDecoration(
                                              alignLabelWithHint: true,
                                              labelText: 'Kategori Pengeluaran',
                                              hintText: 'Pilih Kategori',
                                              isDense: true,
                                              enabledBorder: enabledBorder,
                                              focusedBorder: focusedBorder,
                                              focusedErrorBorder: focusedError,
                                              labelStyle: secondBlackTextFont,
                                              floatingLabelStyle:
                                                  secondBlackTextFont,
                                              hintStyle: greyTextFont.copyWith(
                                                fontSize: sy(10),
                                              ),
                                              suffixIcon: Icon(
                                                Icons.arrow_right_rounded,
                                                size: 25,
                                                color: blackColor,
                                              ),
                                            ),
                                            onTap: () {
                                              Get.toNamed(Routes.CategoryView,
                                                  arguments: false);
                                            },
                                          ),
                                          TextFormField(
                                            enableInteractiveSelection: false,
                                            readOnly: true,
                                            controller:
                                                _.txtTanggalController.value,
                                            onFieldSubmitted: (value) {},
                                            validator: (value) {},
                                            decoration: InputDecoration(
                                              alignLabelWithHint: true,
                                              labelText: 'Tanggal',
                                              hintText: 'Pilih Tanggal',
                                              isDense: true,
                                              enabledBorder: enabledBorder,
                                              focusedBorder: focusedBorder,
                                              focusedErrorBorder: focusedError,
                                              labelStyle: secondBlackTextFont,
                                              floatingLabelStyle:
                                                  secondBlackTextFont,
                                              hintStyle: greyTextFont.copyWith(
                                                fontSize: sy(10),
                                              ),
                                              suffixIcon: Icon(
                                                Icons.arrow_right_rounded,
                                                size: 25,
                                                color: blackColor,
                                              ),
                                            ),
                                            onTap: () {
                                              showDatePicker(
                                                context: context,
                                                initialDate: DateTime.now(),
                                                lastDate: DateTime.now(),
                                                firstDate: DateTime(2021),
                                                builder: (BuildContext? context,
                                                    Widget? child) {
                                                  return Theme(
                                                    data: ThemeData.light()
                                                        .copyWith(
                                                      primaryColor: mainColor,
                                                      buttonTheme:
                                                          ButtonThemeData(
                                                        textTheme:
                                                            ButtonTextTheme
                                                                .primary,
                                                      ),
                                                      colorScheme:
                                                          ColorScheme.light(
                                                        primary: mainColor,
                                                      ).copyWith(
                                                              secondary:
                                                                  blackColor),
                                                    ),
                                                    child: child!,
                                                  );
                                                },
                                              ).then(
                                                (date) {
                                                  if (date != null) {
                                                    _.txtTanggalController.value
                                                            .text =
                                                        DateFormat('yyyy-MM-dd')
                                                            .format(date);
                                                  }
                                                },
                                              );
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Spacer(),
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                      vertical: defaultVerticalMargin,
                                      horizontal: defaultHorizontalMargin,
                                    ),
                                    height: sy(45),
                                    width: double.infinity - 10,
                                    child: TextButton(
                                      style: TextButton.styleFrom(
                                        alignment: Alignment.center,
                                        backgroundColor: mainColor,
                                        elevation: 0,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        primary: whiteColor,
                                      ),
                                      child:
                                          (_.isLoadingCreateTransaksiPengeluaran
                                                      .value ==
                                                  false)
                                              ? Text(
                                                  "Tambah Pengeluaran",
                                                  style: whiteTextFont.copyWith(
                                                    fontSize: sy(12),
                                                  ),
                                                )
                                              : SizedBox(
                                                  height: sy(15),
                                                  width: sy(15),
                                                  child:
                                                      CircularProgressIndicator(
                                                    color: whiteColor,
                                                  ),
                                                ),
                                      onPressed: () {
                                        if (_.isLoadingCreateTransaksiPengeluaran
                                                .value ==
                                            false) {
                                          _onClickTambahPengeluaran();
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              ),
                              //* BATAS TAB PAGER
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Material(
                                    elevation: 4,
                                    borderRadius: BorderRadius.circular(4),
                                    color: Colors.white,
                                    child: Container(
                                      padding: EdgeInsets.only(
                                        left: 10,
                                        right: 10,
                                        top: 10,
                                        bottom: 30,
                                      ),
                                      child: Column(
                                        children: [
                                          TextFormField(
                                            keyboardType: TextInputType.number,
                                            controller: _.txtNominalController2,
                                            focusNode: _.txtNominalFocusNode2,
                                            textInputAction:
                                                TextInputAction.next,
                                            onFieldSubmitted: (value) {
                                              Methods.txtFieldFocusChange(
                                                context,
                                                _.txtNominalFocusNode2,
                                                _.txtNamaFocusNode2,
                                              );
                                            },
                                            validator: (value) {
                                              if (isNull(value)) {
                                                return 'Nominal harus di isi';
                                              } else if (double.parse(
                                                      transactionMasterController
                                                          .txtNominalController2
                                                          .text
                                                          .split('.')
                                                          .join("")) ==
                                                  0) {
                                                return 'Nominal harus di isi';
                                              } else if (double.parse(
                                                      transactionMasterController
                                                          .txtNominalController2
                                                          .text
                                                          .split('.')
                                                          .join("")) >
                                                  999999) {
                                                return 'Maksimal Nominal Rp 999.999';
                                              } else if (double.parse(
                                                      transactionMasterController
                                                          .txtNominalController2
                                                          .text
                                                          .split('.')
                                                          .join("")) <
                                                  100) {
                                                return 'Minimal Nominal Rp 100';
                                              }
                                            },
                                            decoration: InputDecoration(
                                              alignLabelWithHint: true,
                                              labelText: 'Nominal',
                                              hintText: 'Masukkan nominal',
                                              isDense: true,
                                              errorStyle:
                                                  errorTextFont.copyWith(
                                                fontSize: sy(8),
                                                height: 0.6,
                                              ),
                                              enabledBorder: enabledBorder,
                                              focusedBorder: focusedBorder,
                                              errorBorder: errorBorder,
                                              focusedErrorBorder: focusedError,
                                              labelStyle: secondBlackTextFont,
                                              floatingLabelStyle:
                                                  secondBlackTextFont,
                                              hintStyle: greyTextFont.copyWith(
                                                  fontSize: sy(10)),
                                            ),
                                          ),
                                          TextFormField(
                                            controller: _.txtNamaController2,
                                            focusNode: _.txtNamaFocusNode2,
                                            textInputAction:
                                                TextInputAction.done,
                                            onFieldSubmitted: (value) {
                                              FocusScope.of(context).unfocus();
                                            },
                                            validator: (value) {},
                                            decoration: InputDecoration(
                                              alignLabelWithHint: true,
                                              labelText: 'Nama Pemasukan',
                                              hintText:
                                                  'Masukkan nama pemasukan',
                                              isDense: true,
                                              enabledBorder: enabledBorder,
                                              focusedBorder: focusedBorder,
                                              labelStyle: secondBlackTextFont,
                                              floatingLabelStyle:
                                                  secondBlackTextFont,
                                              hintStyle: greyTextFont.copyWith(
                                                  fontSize: sy(10)),
                                            ),
                                          ),
                                          SizedBox(
                                            height: sy(5),
                                          ),
                                          TextFormField(
                                            enableInteractiveSelection: false,
                                            readOnly: true,
                                            controller:
                                                _.txtKategoriController2.value,
                                            onFieldSubmitted: (value) {},
                                            validator: (value) {},
                                            decoration: InputDecoration(
                                              alignLabelWithHint: true,
                                              labelText: 'Kategori Pemasukan',
                                              hintText: 'Pilih Kategori',
                                              isDense: true,
                                              enabledBorder: enabledBorder,
                                              focusedBorder: focusedBorder,
                                              focusedErrorBorder: focusedError,
                                              labelStyle: secondBlackTextFont,
                                              floatingLabelStyle:
                                                  secondBlackTextFont,
                                              hintStyle: greyTextFont.copyWith(
                                                fontSize: sy(10),
                                              ),
                                              suffixIcon: Icon(
                                                Icons.arrow_right_rounded,
                                                size: 25,
                                                color: blackColor,
                                              ),
                                            ),
                                            onTap: () {
                                              Get.toNamed(Routes.CategoryView,
                                                  arguments: true);
                                            },
                                          ),
                                          TextFormField(
                                            enableInteractiveSelection: false,
                                            readOnly: true,
                                            controller:
                                                _.txtTanggalController2.value,
                                            onFieldSubmitted: (value) {},
                                            validator: (value) {},
                                            decoration: InputDecoration(
                                              alignLabelWithHint: true,
                                              labelText: 'Tanggal',
                                              hintText: 'Pilih Tanggal',
                                              isDense: true,
                                              enabledBorder: enabledBorder,
                                              focusedBorder: focusedBorder,
                                              focusedErrorBorder: focusedError,
                                              labelStyle: secondBlackTextFont,
                                              floatingLabelStyle:
                                                  secondBlackTextFont,
                                              hintStyle: greyTextFont.copyWith(
                                                fontSize: sy(10),
                                              ),
                                              suffixIcon: Icon(
                                                Icons.arrow_right_rounded,
                                                size: 25,
                                                color: blackColor,
                                              ),
                                            ),
                                            onTap: () {
                                              showDatePicker(
                                                context: context,
                                                initialDate: DateTime.now(),
                                                lastDate: DateTime.now(),
                                                firstDate: DateTime(2021),
                                                builder: (BuildContext? context,
                                                    Widget? child) {
                                                  return Theme(
                                                    data: ThemeData.light()
                                                        .copyWith(
                                                      primaryColor: mainColor,
                                                      buttonTheme:
                                                          ButtonThemeData(
                                                        textTheme:
                                                            ButtonTextTheme
                                                                .primary,
                                                      ),
                                                      colorScheme:
                                                          ColorScheme.light(
                                                        primary: mainColor,
                                                      ).copyWith(
                                                              secondary:
                                                                  blackColor),
                                                    ),
                                                    child: child!,
                                                  );
                                                },
                                              ).then(
                                                (date) {
                                                  if (date != null) {
                                                    _.txtTanggalController2
                                                            .value.text =
                                                        DateFormat('yyyy-MM-dd')
                                                            .format(date);
                                                  }
                                                },
                                              );
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Spacer(),
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                      vertical: defaultVerticalMargin,
                                      horizontal: defaultHorizontalMargin,
                                    ),
                                    height: sy(45),
                                    width: double.infinity - 10,
                                    child: TextButton(
                                      style: TextButton.styleFrom(
                                        alignment: Alignment.center,
                                        backgroundColor: mainColor,
                                        elevation: 0,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        primary: whiteColor,
                                      ),
                                      child:
                                          (_.isLoadingCreateTransaksiPemasukan
                                                      .value ==
                                                  false)
                                              ? Text(
                                                  "Tambah Pemasukan",
                                                  style: whiteTextFont.copyWith(
                                                    fontSize: sy(12),
                                                  ),
                                                )
                                              : SizedBox(
                                                  height: sy(15),
                                                  width: sy(15),
                                                  child:
                                                      CircularProgressIndicator(
                                                    color: whiteColor,
                                                  ),
                                                ),
                                      onPressed: () {
                                        if (_.isLoadingCreateTransaksiPengeluaran
                                                .value ==
                                            false) {
                                          _onClickTambahPemasukan();
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
      },
    );
  }
}
