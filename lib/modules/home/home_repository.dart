import 'package:dio/dio.dart';
import 'package:starter_1/models/keuangan.dart';
import 'package:starter_1/models/keuangan2.dart';
import 'package:starter_1/shared/constant.dart';

class HomeRepository {
  Future<Keuangan?> laporanKeuanganLineRequest({
    int? userId,
    String? token,
    String? tanggalMulai,
    String? tanggalSelesai,
    String? tipe,
  }) async {
    // Variables
    Response response;

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };

    BaseOptions options = BaseOptions(
      headers: headers,
      connectTimeout: 10000,
    );

    try {
      response = await Dio(options).get(
        "$API/dashboard/grafik_garis",
        queryParameters: {
          "user_id": "$userId",
          "tipe": "$tipe",
          "tanggal_mulai": "$tanggalMulai",
          "tanggal_selesai": "$tanggalSelesai",
        },
      );

      print('HEHE LINE == ${response.data}');
      print('[SUCCES] - Get Laporan Keuangan LINE');

      return Keuangan.fromJson(response.data);
    } on DioError catch (e) {
      print("[ERROR] - Get Laporan Keuangan LINE == ${e.toString()}");

      if (e.type == DioErrorType.connectTimeout) {
        throw "Connection Timeout Coba Lagi";
      } else {
        Keuangan keuangan = Keuangan.fromJson(e.response!.data);

        throw keuangan.message!;
      }
    }
  }

  Future<Keuangan2?> laporanKeuanganPieRequest({
    int? userId,
    String? token,
    String? tanggalMulai,
    String? tanggalSelesai,
  }) async {
    // Variables
    Response response;

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };

    BaseOptions options = BaseOptions(
      headers: headers,
      connectTimeout: 10000,
    );

    try {
      response = await Dio(options).get(
        "$API/dashboard/laporan_keuangan",
        queryParameters: {
          "user_id": "$userId",
          "tanggal_mulai": "$tanggalMulai",
          "tanggal_selesai": "$tanggalSelesai",
        },
      );

      print('HEHE PIE == ${response.data}');

      print('[SUCCES] - Get Laporan Keuangan PIE');

      return Keuangan2.fromJson(response.data);
    } on DioError catch (e) {
      print("[ERROR] - Get Laporan Keuangan PIE == ${e.toString()}");

      if (e.type == DioErrorType.connectTimeout) {
        throw "Connection Timeout Coba Lagi";
      } else {
        Keuangan2 keuangan2 = Keuangan2.fromJson(e.response!.data);

        throw keuangan2.message!;
      }
    }
  }
}
