// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bantuan.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Bantuan _$BantuanFromJson(Map<String, dynamic> json) => Bantuan(
      data: json['data'] == null
          ? null
          : BantuanList.fromJson(json['data'] as Map<String, dynamic>),
      message: json['message'] as String?,
      status: json['status'] as String?,
      status_code: json['status_code'] as int?,
    );

Map<String, dynamic> _$BantuanToJson(Bantuan instance) => <String, dynamic>{
      'data': instance.data?.toJson(),
      'message': instance.message,
      'status': instance.status,
      'status_code': instance.status_code,
    };

BantuanList _$BantuanListFromJson(Map<String, dynamic> json) => BantuanList(
      total_items: json['total_items'] as int?,
      list: (json['list'] as List<dynamic>?)
          ?.map((e) => BantuanItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$BantuanListToJson(BantuanList instance) =>
    <String, dynamic>{
      'total_items': instance.total_items,
      'list': instance.list?.map((e) => e.toJson()).toList(),
    };

BantuanItem _$BantuanItemFromJson(Map<String, dynamic> json) => BantuanItem(
      id: json['id'] as int?,
      judul: json['judul'] as String?,
      keterangan: json['keterangan'] as String?,
      link_v: json['link_v'] as String?,
      link_youtube: json['link_youtube'] as String?,
    );

Map<String, dynamic> _$BantuanItemToJson(BantuanItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'judul': instance.judul,
      'keterangan': instance.keterangan,
      'link_v': instance.link_v,
      'link_youtube': instance.link_youtube,
    };
