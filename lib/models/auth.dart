import 'package:json_annotation/json_annotation.dart';

part 'auth.g.dart';

// ignore_for_file: non_constant_identifier_names

@JsonSerializable(explicitToJson: true)
class Auth {
  User? data;
  String? message;
  String? status;
  int? status_code;

  Auth({
    this.data,
    this.message,
    this.status,
    this.status_code,
  });

  factory Auth.fromJson(Map<String, dynamic> parsedJson) =>
      _$AuthFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$AuthToJson(this);
}

@JsonSerializable(explicitToJson: true)
class User {
  int? id;
  String? name;
  String? email;
  String? username;
  String? password;
  String? file;
  String? token;
  String? token_fcm;
  String? url;

  User({
    this.id,
    this.name,
    this.email,
    this.username,
    this.password,
    this.file,
    this.token,
    this.token_fcm,
    this.url,
  });

  factory User.fromJson(Map<String, dynamic> parsedJson) =>
      _$UserFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
