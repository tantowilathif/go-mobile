import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

//* Instance Top-Variables
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

class NotificationConfig {
  //* Other Variables
  static String? notifImage = 'ic_inisa_notification';
  static String? notifChannelId = 'default_notification_channel_id';
  static String? notifChannelName = 'default_notification_channel_name';
  static String? notifChanelDesc = 'default_notification_channel_desc';

  //* Config Variables
  static final InitializationSettings initializationSettings =
      InitializationSettings(
    android: AndroidInitializationSettings('$notifImage'),
    iOS: IOSInitializationSettings(
      onDidReceiveLocalNotification: (
        int? id,
        String? title,
        String? body,
        String? payload,
      ) async {},
    ),
  );

  static final NotificationDetails platformChannelSpecifics =
      NotificationDetails(
    android: AndroidNotificationDetails(
      '$notifChannelId',
      '$notifChannelName',
      channelDescription: '$notifChanelDesc',
      importance: Importance.max,
      priority: Priority.high,
      showWhen: false,
      color: Color(0xffcc1d15),
    ),
    iOS: IOSNotificationDetails(),
  );

  //* Methods (Function)
  static Future init() async {
    //* Set the background messaging handler early on, as a named top-level function
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(
          AndroidNotificationChannel(
            '$notifChannelId',
            '$notifChannelName',
            description: '$notifChanelDesc', // description,
            importance: Importance.max,
          ),
        );

    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: (String? payload) async {
        if (payload != null) {
          debugPrint(
              "[Flutter Local Notification][initialize] MASUK ON SELECT NOTIFIICATION");
        }
      },
    );

    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      debugPrint(
          "[Firebase Messaging][onMessage] MASUK FIREBASE ONMESSAGE LISTEN");
      onMessageHandler(message);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      debugPrint(
          "[Firebase Messaging][onMessageOpenedApp] MASUK FIREBASE ON MESSAGE OPENES APP LISTEN");
      onMessageHandler(message);
    });

    FirebaseMessaging.onBackgroundMessage(
        (message) async => onMessageHandler(message));
  }

  static void onMessageHandler(RemoteMessage message) async {
    debugPrint("MASUK SHOW LOCAL");
    await flutterLocalNotificationsPlugin.show(
      DateTime.now().millisecond,
      message.notification!.title,
      message.notification!.body,
      platformChannelSpecifics,
      payload: 'test',
    );
  }
}
