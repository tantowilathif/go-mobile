// ignore_for_file: prefer_final_fields, must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:starter_1/modules/category/category_controller.dart';
import 'package:starter_1/shared/themes.dart';
import 'package:validators/validators.dart';

class CategoryMasterView extends StatelessWidget {
  CategoryMasterView({Key? key}) : super(key: key);

  //* Text Editing Controllers
  TextEditingController _txtNamaCategoryController = TextEditingController();

  //* Variables
  bool? isWannaUpdate;
  bool? isFromMasuk;
  String? categoryName;
  int? categoryId;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    CategoryController _categoryController = Get.put(CategoryController());

    _onClickTambahMasuk() {
      if (_formKey.currentState!.validate()) {
        _formKey.currentState!.save();

        _categoryController.createCategoryMasuk(
            categoryName: _txtNamaCategoryController.text);
      }
    }

    _onClickTambahKeluar() {
      if (_formKey.currentState!.validate()) {
        _formKey.currentState!.save();

        _categoryController.createCategoryKeluar(
            categoryName: _txtNamaCategoryController.text);
      }
    }

    _onClickUpdate() {
      if (_formKey.currentState!.validate()) {
        _formKey.currentState!.save();

        _categoryController.updateCategory(
          idCategory: categoryId,
          categoryName: _txtNamaCategoryController.text,
          isFromMasuk: isFromMasuk,
        );
      }
    }

    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return GetX<CategoryController>(
          initState: (_) {
            if (Get.arguments != null) {
              isFromMasuk = Get.arguments[0];
              isWannaUpdate = Get.arguments[1];
              categoryName = Get.arguments[2] ?? '';
              _txtNamaCategoryController.text = Get.arguments[2] ?? '';
              categoryId = Get.arguments[3];
            }
          },
          builder: (_) {
            return Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: AppBar(
                title: Text(
                  (isWannaUpdate == true)
                      ? 'Update Kategori $categoryName'
                      : (isFromMasuk == true)
                          ? 'Tambah Kategori Masuk'
                          : 'Tambah Kategori Keluar',
                ),
                centerTitle: true,
              ),
              body: InkWell(
                onTap: () {
                  FocusScope.of(context).unfocus();
                },
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                focusColor: Colors.transparent,
                child: Container(
                  padding: EdgeInsets.symmetric(
                    vertical: defaultVerticalMargin,
                    horizontal: defaultHorizontalMargin,
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          controller: _txtNamaCategoryController,
                          textInputAction: TextInputAction.done,
                          onFieldSubmitted: (value) {},
                          validator: (value) {
                            if (isNull(value)) {
                              return 'Nama harus di isi';
                            }
                          },
                          decoration: InputDecoration(
                            alignLabelWithHint: true,
                            labelText: 'Nama Kategori',
                            hintText: 'Masukkan nama kategori',
                            isDense: true,
                            errorStyle: errorTextFont.copyWith(
                              fontSize: sy(8),
                              height: 0.6,
                            ),
                            enabledBorder: enabledBorder,
                            focusedBorder: focusedBorder,
                            errorBorder: errorBorder,
                            focusedErrorBorder: focusedError,
                            labelStyle: secondBlackTextFont,
                            floatingLabelStyle: secondBlackTextFont,
                            hintStyle: greyTextFont.copyWith(
                              fontSize: sy(10),
                            ),
                          ),
                        ),
                        Spacer(),
                        Container(
                          padding: EdgeInsets.symmetric(
                            vertical: defaultVerticalMargin,
                            horizontal: defaultHorizontalMargin,
                          ),
                          height: sy(45),
                          width: double.infinity,
                          child: TextButton(
                            style: TextButton.styleFrom(
                              alignment: Alignment.center,
                              backgroundColor: mainColor,
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              primary: whiteColor,
                            ),
                            child: (_.isLoadingCreateCategory.value == false)
                                ? Text(
                                    (isWannaUpdate == true)
                                        ? 'Update Kategori'
                                        : "Tambah Kategori",
                                    style: whiteTextFont.copyWith(
                                      fontSize: sy(12),
                                    ),
                                  )
                                : SizedBox(
                                    height: sy(15),
                                    width: sy(15),
                                    child: CircularProgressIndicator(
                                      color: whiteColor,
                                    ),
                                  ),
                            onPressed: () {
                              if (_.isLoadingCreateCategory.value == false) {
                                if (isWannaUpdate == true) {
                                  _onClickUpdate();
                                } else {
                                  if (isFromMasuk == true) {
                                    _onClickTambahMasuk();
                                  } else {
                                    _onClickTambahKeluar();
                                  }
                                }
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
