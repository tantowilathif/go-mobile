import 'dart:convert';
import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/models/keuangan.dart';
import 'package:starter_1/models/keuangan2.dart';
import 'package:starter_1/modules/home/home_repository.dart';
import 'package:starter_1/shared/themes.dart';
import 'package:collection/collection.dart';
import 'package:starter_1/utils/internet_connection_check.dart';

class HomeController extends GetxController {
  //* Vairables for conditional view
  var isLoadingGetLaporanKeuangan = false.obs;
  var isLoadingGetLaporanKeuanganPie = false.obs;
  var isGotTimeOutLine = false.obs;
  var isGotTimeOutPie = false.obs;

  var isHarianPicked = false.obs;
  var isMingguanPicked = false.obs;
  var selectedIndexPage = 0.obs;

  var hasConnection = true.obs;

  //* Other Variables
  final HomeRepository _homeRepository = HomeRepository();

  Keuangan? laporanKeuanganLine;
  Keuangan2? laporanKeuanganPie;

  List<double>? pemasukanList = [];
  List<double>? pengeluaranList = [];

  Rxn<double> maxYPengeluaran = Rxn<double>();
  Rxn<double> maxYPemasukan = Rxn<double>();

  Rxn<double> minYPengeluaran = Rxn<double>();
  Rxn<double> minYPemasukan = Rxn<double>();

  Rxn<double> maxY = Rxn<double>();
  Rxn<double> minY = Rxn<double>();
  Rxn<double> maxX = Rxn<double>();

  Rxn<double> onePercentValue = Rxn<double>();
  Rxn<double> sectionValuePemasukan = Rxn<double>();
  Rxn<double> sectionValuePengeluaran = Rxn<double>();
  Rxn<String> sectionTittlePemasukan = Rxn<String>();
  Rxn<String> sectionTittlePengeluaran = Rxn<String>();

  LineChartBarData lineChartBarDataPemasukan = LineChartBarData();
  LineChartBarData lineChartBarDataPengeluaran = LineChartBarData();
  FlTitlesData titlesData1 = FlTitlesData();
  List<LineChartBarData> lineChartBarData1 = [];
  List<PieChartSectionData> pieChartSectionData = [];

  //* Cycles
  @override
  void onInit() async {
    super.onInit();

    hasConnection.value =
        await InternetConnectionCheck.getInstance().checkConnectivity();

    InternetConnectionCheck.getInstance().connectionChange.listen(
      (event) {
        connectionChanged(event);
      },
    );

    if (hasConnection.value == false) {
      isGotTimeOutLine.value = true;
      isGotTimeOutPie.value = true;
    }
  }

  //* Methods (Function)
  void connectionChanged(dynamic hasConnectionStatus) {
    hasConnection.value = hasConnectionStatus;
  }

  SideTitles getBottomTitles(List<String> labelValue, String isMingguan) {
    return SideTitles(
      showTitles: true,
      reservedSize: 20,
      margin: 10,
      interval: 1,
      getTextStyles: (context, value) => TextStyle(
        color: mainColor,
        fontSize: 12,
      ),
      getTitles: (value) {
        if (isMingguan != 'mingguan') {
          if (value.toInt() == 0) {
            return labelValue[0];
          } else if (value.toInt() == 1) {
            return labelValue[1];
          } else if (value.toInt() == 2) {
            return labelValue[2];
          } else if (value.toInt() == 3) {
            return labelValue[3];
          } else if (value.toInt() == 4) {
            return labelValue[4];
          } else if (value.toInt() == 5) {
            return labelValue[5];
          } else if (value.toInt() == 6) {
            return labelValue[6];
          } else if (value.toInt() == 7) {
            return labelValue[7];
          } else {
            return '';
          }
        } else {
          if (labelValue.length == 5) {
            if (value.toInt() == 0) {
              var _formattedText =
                  labelValue[0].substring(labelValue[0].length - 1);
              return "W$_formattedText";
            } else if (value.toInt() == 1) {
              var _formattedText =
                  labelValue[1].substring(labelValue[1].length - 1);
              return "W$_formattedText";
            } else if (value.toInt() == 2) {
              var _formattedText =
                  labelValue[2].substring(labelValue[2].length - 1);
              return "W$_formattedText";
            } else if (value.toInt() == 3) {
              var _formattedText =
                  labelValue[3].substring(labelValue[3].length - 1);
              return "W$_formattedText";
            } else if (value.toInt() == 4) {
              var _formattedText =
                  labelValue[4].substring(labelValue[4].length - 1);
              return "W$_formattedText";
            } else {
              return '';
            }
          } else if (labelValue.length == 6) {
            if (value.toInt() == 0) {
              var _formattedText =
                  labelValue[0].substring(labelValue[0].length - 1);
              return "W$_formattedText";
            } else if (value.toInt() == 1) {
              var _formattedText =
                  labelValue[1].substring(labelValue[1].length - 1);
              return "W$_formattedText";
            } else if (value.toInt() == 2) {
              var _formattedText =
                  labelValue[2].substring(labelValue[2].length - 1);
              return "W$_formattedText";
            } else if (value.toInt() == 3) {
              var _formattedText =
                  labelValue[3].substring(labelValue[3].length - 1);
              return "W$_formattedText";
            } else if (value.toInt() == 4) {
              var _formattedText =
                  labelValue[4].substring(labelValue[4].length - 1);
              return "W$_formattedText";
            } else if (value.toInt() == 5) {
              var _formattedText =
                  labelValue[5].substring(labelValue[5].length - 1);
              return "W$_formattedText";
            } else {
              return '';
            }
          } else {
            if (value.toInt() == 0) {
              var _formattedText =
                  labelValue[0].substring(labelValue[0].length - 1);
              return "W$_formattedText";
            } else if (value.toInt() == 1) {
              var _formattedText =
                  labelValue[1].substring(labelValue[1].length - 1);
              return "W$_formattedText";
            } else if (value.toInt() == 2) {
              var _formattedText =
                  labelValue[2].substring(labelValue[2].length - 1);
              return "W$_formattedText";
            } else if (value.toInt() == 3) {
              var _formattedText =
                  labelValue[3].substring(labelValue[3].length - 1);
              return "W$_formattedText";
            } else {
              return '';
            }
          }
        }
      },
    );
  }

  SideTitles getLeftTitles(double max, double min) {
    return SideTitles(
      showTitles: true,
      reservedSize: 30,
      margin: 10,
      interval: 1,
      getTextStyles: (context, value) => TextStyle(
        color: mainDarkColor,
        fontSize: 12,
      ),
      getTitles: (value) {
        if (value == max.toInt()) {
          var _formattedNumber = NumberFormat.compactCurrency(
            symbol: '',
          ).format(value * 1000.0);

          return _formattedNumber;
        } else if (value == min.toInt()) {
          var _formattedNumber = NumberFormat.compactCurrency(
            decimalDigits: 1,
            symbol: '',
          ).format(value * 1000.0);

          return _formattedNumber;
        } else if (value == max.toInt() / 2) {
          var _formattedNumber = NumberFormat.compactCurrency(
            decimalDigits: 1,
            symbol: '',
          ).format(value * 1000.0);
          return _formattedNumber;
        } else {
          return '';
        }
      },
    );
  }

  List<FlSpot> spotPemasukan(List<double> listPemasukan) {
    List<FlSpot> value = [];

    for (var i = 0; i < listPemasukan.length; i++) {
      value.add(
        FlSpot(
          i.toDouble(),
          listPemasukan[i],
        ),
      );
    }

    return value;
  }

  List<FlSpot> spotPengeluaran(List<double> listPengeluaran) {
    List<FlSpot> value = [];

    for (var i = 0; i < listPengeluaran.length; i++) {
      value.add(
        FlSpot(i.toDouble(), listPengeluaran[i]),
      );
    }

    return value;
  }

  void onBottomItemNavTapped(index) {
    selectedIndexPage.value = index;
  }

  void filterStatus(int value) {
    if (value == 0) {
      isMingguanPicked.value = false;

      var now = DateTime.now();

      var tanggalMulaiValue = DateFormat('yyyy-MM-dd').format(
        DateTime(now.year, now.month, now.day - 7),
      );
      var tanggalSelesaiValue = DateFormat('yyyy-MM-dd').format(DateTime.now());

      if (hasConnection.value == true) {
        getLaporanKeuangan(
          tanggalMulai: tanggalMulaiValue,
          tanggalSelesai: tanggalSelesaiValue,
          tipe: 'harian',
        );

        getLaporanKeuanganPie(
          tanggalMulai: tanggalMulaiValue,
          tanggalSelesai: tanggalSelesaiValue,
        );
      } else {
        Get.snackbar('Error', 'Check your internet connection');

        isLoadingGetLaporanKeuanganPie.value = false;
        isLoadingGetLaporanKeuangan.value = false;
      }
    } else if (value == 1) {
      isHarianPicked.value = false;

      var now = DateTime.now();

      var tanggalMulaiValue = DateFormat('yyyy-MM-dd').format(
        DateTime(now.year, now.month, now.day - 30),
      );
      var tanggalSelesaiValue = DateFormat('yyyy-MM-dd').format(now);

      if (hasConnection.value == true) {
        getLaporanKeuangan(
          tanggalMulai: tanggalMulaiValue,
          tanggalSelesai: tanggalSelesaiValue,
          tipe: 'mingguan',
        );

        getLaporanKeuanganPie(
          tanggalMulai: tanggalMulaiValue,
          tanggalSelesai: tanggalSelesaiValue,
        );
      } else {
        Get.snackbar('Error', 'Check your internet connection');

        isLoadingGetLaporanKeuanganPie.value = false;
        isLoadingGetLaporanKeuangan.value = false;
      }
    }

    update();
  }

  Future<void> getLaporanKeuangan(
      {String? tanggalMulai, String? tanggalSelesai, String? tipe}) async {
    // Variables
    User? userData;

    isLoadingGetLaporanKeuangan.value = true;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    try {
      laporanKeuanganLine = await _homeRepository.laporanKeuanganLineRequest(
        userId: userData.id,
        token: userData.token,
        tipe: tipe,
        tanggalMulai: tanggalMulai,
        tanggalSelesai: tanggalSelesai,
      );

      pengeluaranList = laporanKeuanganLine!.data!.pengeluaran;
      pemasukanList = laporanKeuanganLine!.data!.pemasukan;

      if (pengeluaranList!.sum == 0) {
        if (pemasukanList!.sum == 0) {
          maxX.value = pengeluaranList!.length.toDouble();
          maxY.value = 0;
          minY.value = 0;
        } else {
          maxX.value = pengeluaranList!.length.toDouble();
          maxY.value = pemasukanList!.reduce(max).toDouble();
          minY.value = 0;
        }
      } else {
        maxX.value = pengeluaranList!.length.toDouble();

        minYPemasukan.value = pemasukanList!.reduce(min).toDouble();
        minYPengeluaran.value = pengeluaranList!.reduce(min).toDouble();

        maxYPengeluaran.value = pengeluaranList!.reduce(max).toDouble();
        maxYPemasukan.value = pemasukanList!.reduce(max).toDouble();

        if (maxYPemasukan.value! > maxYPengeluaran.value!) {
          maxY.value = maxYPemasukan.value;
        } else {
          maxY.value = maxYPengeluaran.value;
        }

        if (minYPemasukan.value! < minYPengeluaran.value!) {
          minY.value = minYPemasukan.value;
        } else {
          minY.value = minYPengeluaran.value;
        }
      }

      print('GRAFIK == $maxX - $maxY -- $minY');

      lineChartBarDataPengeluaran = LineChartBarData(
        isCurved: false,
        colors: [errorColor],
        barWidth: 1,
        isStrokeCapRound: true,
        dotData: FlDotData(
          show: true,
          checkToShowDot: (spot, barData) {
            for (var i = 0; i < maxX.value!; i++) {
              if (spot.y == 0.0 && spot.x == i) {
                return false;
              }
            }

            return true;
          },
        ),
        belowBarData: BarAreaData(show: false),
        spots: spotPengeluaran(pengeluaranList!),
      );

      lineChartBarDataPemasukan = LineChartBarData(
        isCurved: false,
        colors: [greenColor],
        barWidth: 2.5,
        isStrokeCapRound: true,
        dotData: FlDotData(
          show: true,
          checkToShowDot: (spot, barData) {
            for (var i = 0; i < maxX.value!; i++) {
              if (spot.y == 0.0 && spot.x == i) {
                return false;
              }
            }

            return true;
          },
        ),
        belowBarData: BarAreaData(show: false),
        spots: spotPemasukan(pemasukanList!),
      );

      titlesData1 = FlTitlesData(
        rightTitles: SideTitles(showTitles: false),
        topTitles: SideTitles(showTitles: false),
        bottomTitles: getBottomTitles(laporanKeuanganLine!.data!.label!, tipe!),
        leftTitles: getLeftTitles(maxY.value!, minY.value!),
      );

      lineChartBarData1 = [
        lineChartBarDataPemasukan,
        lineChartBarDataPengeluaran,
      ];

      isGotTimeOutLine.value = false;

      Future.delayed(
        Duration(seconds: 1),
        () => isLoadingGetLaporanKeuangan.value = false,
      );
    } catch (e) {
      isLoadingGetLaporanKeuangan.value = false;

      if (e.toString() == "Connection Timeout Coba Lagi") {
        isGotTimeOutLine.value = true;

        Get.snackbar('Timeout', 'Silahkan Coba Lagi');
      } else {
        Get.snackbar('Error', 'Something wrong please try again');
      }
    }

    update();
  }

  Future<void> getLaporanKeuanganPie(
      {String? tanggalMulai, String? tanggalSelesai}) async {
    // Variables
    User? userData;

    isLoadingGetLaporanKeuanganPie.value = true;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    try {
      laporanKeuanganPie = await _homeRepository.laporanKeuanganPieRequest(
        userId: userData.id,
        token: userData.token,
        tanggalMulai: tanggalMulai,
        tanggalSelesai: tanggalSelesai,
      );

      if (laporanKeuanganPie!.data!.total != 0) {
        onePercentValue.value = laporanKeuanganPie!.data!.total! / 100;

        if (laporanKeuanganPie!.data!.pemasukan != 0) {
          sectionValuePemasukan.value =
              laporanKeuanganPie!.data!.pemasukan! / onePercentValue.value!;
          sectionTittlePemasukan.value =
              sectionValuePemasukan.value!.round().toString();
        } else {
          sectionValuePemasukan.value = 0.0;
          sectionTittlePemasukan.value = '0';
        }

        if (laporanKeuanganPie!.data!.pengeluaran != 0) {
          sectionValuePengeluaran.value =
              laporanKeuanganPie!.data!.pengeluaran! / onePercentValue.value!;
          sectionTittlePengeluaran.value =
              sectionValuePengeluaran.value!.round().toString();
        } else {
          sectionValuePengeluaran.value = 0.0;
          sectionTittlePengeluaran.value = '0';
        }

        pieChartSectionData = [
          PieChartSectionData(
            color: greenColor,
            value: sectionValuePemasukan.value,
            title: '${sectionTittlePemasukan.value}%',
            radius: 50.0,
            titleStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: whiteColor,
            ),
          ),
          PieChartSectionData(
            color: errorColor,
            value: sectionValuePengeluaran.value,
            title: '${sectionTittlePengeluaran.value}%',
            radius: 50.0,
            titleStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: whiteColor,
            ),
          ),
        ];
      } else {
        onePercentValue.value = 0.0;
      }

      isGotTimeOutPie.value = false;

      Future.delayed(
        Duration(seconds: 1),
        () => isLoadingGetLaporanKeuanganPie.value = false,
      );
    } catch (e) {
      isLoadingGetLaporanKeuanganPie.value = false;
      if (e.toString() == "Connection Timeout Coba Lagi") {
        isGotTimeOutPie.value = true;

        Get.snackbar('Timeout', 'Silahkan Coba Lagi');
      } else {
        Get.snackbar('Error', 'Something wrong please try again');
      }
    }

    update();
  }
}
