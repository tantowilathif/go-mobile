import 'dart:convert';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:starter_1/config/routes.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/modules/login/login_repository.dart';
import 'package:starter_1/utils/internet_connection_check.dart';

class LoginController extends GetxController {
  //* Vairables for conditional view
  var isLoadingLogin = false.obs;
  var hasConnection = true.obs;

  //* Other Variables
  Auth? authResults;
  final LoginRepository _loginRepository = LoginRepository();

  //* Cycles
  @override
  void onInit() async {
    super.onInit();

    hasConnection.value =
        await InternetConnectionCheck.getInstance().checkConnectivity();

    InternetConnectionCheck.getInstance().connectionChange.listen(
      (event) {
        connectionChanged(event);
      },
    );
  }

  //* Methods (Function)
  void connectionChanged(dynamic hasConnectionStatus) {
    hasConnection.value = hasConnectionStatus;
  }

  Future<void> login({String? emailorUsername, String? password}) async {
    isLoadingLogin.value = true;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (hasConnection.value == true) {
      try {
        User user = User(
          email: emailorUsername,
          password: password,
        );

        authResults = await _loginRepository.loginRequest(user: user);

        await prefs.setString(
          'user',
          jsonEncode(
            authResults!.data!.toJson(),
          ),
        );

        print('${jsonDecode(prefs.getString('user')!)}');

        isLoadingLogin.value = false;

        Get.offAllNamed(Routes.HomeView);
      } catch (e) {
        isLoadingLogin.value = false;

        if (e.toString() == "Connection Timeout Coba Lagi") {
          Get.snackbar('Timeout', 'Silahkan Coba Lagi');
        } else {
          Get.snackbar('Error', 'Something wrong please try again');
        }
      }
    } else {
      Get.snackbar('Error', 'Check your internet connection');
      isLoadingLogin.value = false;
    }
  }
}
