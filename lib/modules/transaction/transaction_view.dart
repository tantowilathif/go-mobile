import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:starter_1/config/routes.dart';
import 'package:starter_1/models/rekap_transaksi.dart';
import 'package:starter_1/modules/transaction/transaction_controller.dart';
import 'package:starter_1/shared/methods.dart';
import 'package:starter_1/shared/themes.dart';

class TransactionView extends StatelessWidget {
  const TransactionView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    TransactionController _transactionController =
        Get.put(TransactionController());

    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return GetX<TransactionController>(
          initState: (_) {
            _.controller!.isHarianPicked.value = true;
            _.controller!.filterStatus(0);
          },
          builder: (_) {
            return Scaffold(
              floatingActionButton: Material(
                elevation: 2,
                color: mainColor,
                borderRadius: BorderRadius.circular(10),
                child: InkWell(
                  splashColor: greyColor.withOpacity(0.2),
                  onTap: () {
                    Get.toNamed(Routes.TransactionMasterView);
                  },
                  child: SizedBox(
                    height: sy(30),
                    width: MediaQuery.of(context).size.width -
                        (MediaQuery.of(context).size.width * 0.75) +
                        75,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Spacer(),
                        Icon(
                          Icons.add_rounded,
                          color: whiteColor,
                          size: 20,
                        ),
                        Text(
                          'Tambah Transaksi',
                          style: whiteTextFont.copyWith(
                            fontSize: sy(9),
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
              ),
              body: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: defaultHorizontalMargin,
                  vertical: defaultVerticalMargin,
                ),
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Transaksi ${_.filterName.value}',
                        style: darkTextFont.copyWith(
                          fontSize: sy(12),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: sy(10),
                    ),
                    Obx(
                      () => Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Material(
                              elevation: (_.isHarianPicked.value) ? 4 : 1,
                              color: (_.isHarianPicked.value)
                                  ? highlightColor
                                  : null,
                              borderRadius: BorderRadius.circular(8),
                              child: InkWell(
                                splashColor: greyColor.withOpacity(0.2),
                                onTap: () {
                                  _.isHarianPicked.value = true;
                                  _.filterStatus(0);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: (_.isHarianPicked.value)
                                        ? null
                                        : Border.all(color: greyColor),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(6),
                                  child: Text(
                                    'Hari Ini',
                                    style: (_.isHarianPicked.value)
                                        ? whiteTextFont.copyWith(
                                            fontSize: sy(10),
                                            fontWeight: FontWeight.normal,
                                          )
                                        : darkTextFont.copyWith(
                                            fontSize: sy(10),
                                            fontWeight: FontWeight.normal,
                                          ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: sx(6),
                          ),
                          Obx(
                            () => Expanded(
                              child: Material(
                                elevation: (_.isMingguanPicked.value) ? 4 : 1,
                                color: (_.isMingguanPicked.value)
                                    ? highlightColor
                                    : null,
                                borderRadius: BorderRadius.circular(8),
                                child: InkWell(
                                  splashColor: greyColor.withOpacity(0.2),
                                  onTap: () {
                                    _.isMingguanPicked.value = true;
                                    _.filterStatus(1);
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.all(6),
                                    decoration: BoxDecoration(
                                      border: (_.isMingguanPicked.value)
                                          ? null
                                          : Border.all(color: greyColor),
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: Text(
                                      'Minggu Ini',
                                      style: (_.isMingguanPicked.value)
                                          ? whiteTextFont.copyWith(
                                              fontSize: sy(10),
                                              fontWeight: FontWeight.normal,
                                            )
                                          : darkTextFont.copyWith(
                                              fontSize: sy(10),
                                              fontWeight: FontWeight.normal),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: sx(10),
                          ),
                          Expanded(
                            child: Material(
                              elevation: (_.isBulananPicked.value) ? 4 : 1,
                              color: (_.isBulananPicked.value)
                                  ? highlightColor
                                  : null,
                              borderRadius: BorderRadius.circular(8),
                              child: InkWell(
                                splashColor: greyColor.withOpacity(0.2),
                                onTap: () {
                                  _.isBulananPicked.value = true;
                                  _.filterStatus(2);
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(6),
                                  decoration: BoxDecoration(
                                    border: (_.isBulananPicked.value)
                                        ? null
                                        : Border.all(color: greyColor),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  child: Text(
                                    'Bulan Ini',
                                    style: (_.isBulananPicked.value)
                                        ? whiteTextFont.copyWith(
                                            fontSize: sy(10),
                                            fontWeight: FontWeight.normal,
                                          )
                                        : darkTextFont.copyWith(
                                            fontSize: sy(10),
                                            fontWeight: FontWeight.normal),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: sy(10),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Material(
                            elevation: (_.isCustomPeriodePicked.value) ? 4 : 1,
                            color: (_.isCustomPeriodePicked.value)
                                ? highlightColor
                                : null,
                            borderRadius: BorderRadius.circular(8),
                            child: InkWell(
                              splashColor: greyColor.withOpacity(0.2),
                              onTap: () async {
                                _.isCustomPeriodePicked.value = true;
                                _.filterStatus(3);

                                await showDateRangePicker(
                                  context: context,
                                  lastDate: DateTime.now(),
                                  firstDate: DateTime(2021),
                                ).then(
                                  (value) {
                                    _.isCustomDidNotSelected.value = false;

                                    if (value != null) {
                                      _.customStartDate.value =
                                          DateFormat('yyyy-MM-dd')
                                              .format(value.start);

                                      _.customEndDate.value =
                                          DateFormat('yyyy-MM-dd')
                                              .format(value.end);

                                      if (_.hasConnection.value == true) {
                                        _.getRekapTransaksi(
                                            tanggalMulai:
                                                _.customStartDate.value,
                                            tanggalSelesai:
                                                _.customEndDate.value);
                                      } else {
                                        Get.snackbar('Error',
                                            'Check your internet connection');
                                        _.isLoadingGetRekapTransaksi.value =
                                            false;
                                      }
                                    } else {
                                      _.isCustomDidNotSelected.value = true;
                                    }
                                  },
                                );
                              },
                              child: Obx(
                                () => Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(6),
                                  decoration: BoxDecoration(
                                    border: (_.isCustomPeriodePicked.value)
                                        ? null
                                        : Border.all(color: greyColor),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  child: (_.isCustomPeriodePicked.value == true)
                                      ? (_.customStartDate.value == '')
                                          ? Text(
                                              'Custom Periode',
                                              style: (_.isCustomPeriodePicked
                                                      .value)
                                                  ? whiteTextFont.copyWith(
                                                      fontSize: sy(10),
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    )
                                                  : darkTextFont.copyWith(
                                                      fontSize: sy(10),
                                                      fontWeight:
                                                          FontWeight.normal),
                                            )
                                          : Text(
                                              '${_.customStartDate.value} - ${_.customEndDate}',
                                              style: (_.isCustomPeriodePicked
                                                      .value)
                                                  ? whiteTextFont.copyWith(
                                                      fontSize: sy(10),
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    )
                                                  : darkTextFont.copyWith(
                                                      fontSize: sy(10),
                                                      fontWeight:
                                                          FontWeight.normal),
                                            )
                                      : Text(
                                          'Custom Periode',
                                          style: (_.isCustomPeriodePicked.value)
                                              ? whiteTextFont.copyWith(
                                                  fontSize: sy(10),
                                                  fontWeight: FontWeight.normal,
                                                )
                                              : darkTextFont.copyWith(
                                                  fontSize: sy(10),
                                                  fontWeight:
                                                      FontWeight.normal),
                                        ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: sy(10),
                    ),
                    Material(
                      elevation: 4,
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.white,
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: (_.isCustomDidNotSelected.value == true)
                            ? Center(
                                child: Text(
                                  'Tanggal Periode Belum Di Pilih',
                                  style: highlightTextFont.copyWith(
                                    fontSize: sy(10),
                                  ),
                                ),
                              )
                            : (_.isLoadingGetRekapTransaksi.value == true)
                                ? Center(
                                    child: SizedBox(
                                      height: sy(20),
                                      width: sy(20),
                                      child: CircularProgressIndicator(
                                        color: mainColor,
                                      ),
                                    ),
                                  )
                                : Column(
                                    children: [
                                      IntrinsicHeight(
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: GestureDetector(
                                                onTap: () {
                                                  List<ListRekapTransaksi>
                                                      result = [];

                                                  for (var item
                                                      in _.transaksiItemList) {
                                                    if (item.pengeluaran ==
                                                        null) {
                                                      result.add(item);
                                                    }
                                                  }

                                                  Get.toNamed(
                                                    Routes.PemasukanDetailView,
                                                    arguments: [
                                                      result,
                                                      _.filterName.value
                                                    ],
                                                  );
                                                },
                                                child: Column(
                                                  children: [
                                                    Text(
                                                      'Pemasukan',
                                                      style: greenTextFont
                                                          .copyWith(
                                                        fontSize: sy(10),
                                                      ),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    Text(
                                                      Methods.convertToIdr(
                                                          _.pemasukan.value ??
                                                              0.0,
                                                          0),
                                                      style:
                                                          darkTextFont.copyWith(
                                                              fontSize: sy(10),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    SizedBox(
                                                      height: sy(4),
                                                    ),
                                                    Text(
                                                      'Klik Lebih Lengkap',
                                                      style:
                                                          greyTextFont.copyWith(
                                                        fontSize: sy(6),
                                                        fontWeight:
                                                            FontWeight.normal,
                                                      ),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            VerticalDivider(
                                              thickness: 1,
                                              color: greyColor,
                                              width: 20,
                                            ),
                                            Expanded(
                                              child: GestureDetector(
                                                onTap: () {
                                                  List<ListRekapTransaksi>
                                                      result = [];

                                                  for (var item
                                                      in _.transaksiItemList) {
                                                    if (item.pemasukan ==
                                                        null) {
                                                      result.add(item);
                                                    }
                                                  }

                                                  Get.toNamed(
                                                    Routes
                                                        .PengeluaranDetailView,
                                                    arguments: [
                                                      result,
                                                      _.filterName.value
                                                    ],
                                                  );
                                                },
                                                child: Column(
                                                  children: [
                                                    Text(
                                                      'Pengeluaran',
                                                      style: errorTextFont
                                                          .copyWith(
                                                        fontSize: sy(10),
                                                      ),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    Text(
                                                      Methods.convertToIdr(
                                                          _.pengeluaran.value ??
                                                              0.0,
                                                          0),
                                                      style:
                                                          darkTextFont.copyWith(
                                                              fontSize: sy(10),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    SizedBox(
                                                      height: sy(4),
                                                    ),
                                                    Text(
                                                      'Klik Lebih Lengkap',
                                                      style:
                                                          greyTextFont.copyWith(
                                                        fontSize: sy(6),
                                                        fontWeight:
                                                            FontWeight.normal,
                                                      ),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Divider(
                                        thickness: 1,
                                        color: greyColor,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Keuntungan',
                                            style: darkTextFont.copyWith(
                                              fontSize: sy(10),
                                              fontWeight: FontWeight.normal,
                                            ),
                                          ),
                                          Text(
                                            Methods.convertToIdr(
                                                _.keuntungan.value ?? 0.0, 0),
                                            style: (_.keuntungan.value == null)
                                                ? mainTextFont.copyWith(
                                                    fontSize: sy(10),
                                                    fontWeight: FontWeight.bold,
                                                  )
                                                : (_.keuntungan.value!
                                                        .isNegative)
                                                    ? errorTextFont.copyWith(
                                                        fontSize: sy(10),
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      )
                                                    : mainTextFont.copyWith(
                                                        fontSize: sy(10),
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                      ),
                    ),
                    SizedBox(
                      height: sy(8),
                    ),
                    (_.isGotTimeOut.value == true)
                        ? Center(
                            child: TextButton(
                              onPressed: () {
                                if (_.isLoadingGetRekapTransaksi.value ==
                                    false) {
                                  _.isHarianPicked.value = true;
                                  _.filterStatus(0);
                                }
                              },
                              child: Text(
                                'Refresh',
                                style: mainTextFont.copyWith(
                                  fontSize: sy(10),
                                ),
                              ),
                            ),
                          )
                        : Visibility(
                            visible: !_.isCustomDidNotSelected.value,
                            child: Expanded(
                              child: Column(
                                children: [
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      'Swipe ke kiri untuk update transaksi',
                                      style: greyTextFont.copyWith(
                                          fontSize: sy(8)),
                                    ),
                                  ),
                                  Expanded(
                                    child: (_.transaksiItemList.isEmpty)
                                        ? Center(
                                            child: Text(
                                              'No Data',
                                              style: mainTextFont.copyWith(
                                                fontSize: sy(8),
                                              ),
                                            ),
                                          )
                                        : ListView.separated(
                                            itemCount:
                                                _.transaksiItemList.length,
                                            separatorBuilder: (context, index) {
                                              return Divider(
                                                height: 0,
                                                color: greyColor,
                                                // thickness: 0.4,
                                              );
                                            },
                                            itemBuilder: (context, index) {
                                              return Slidable(
                                                key: const ValueKey(1),
                                                endActionPane: ActionPane(
                                                  extentRatio: 0.50,
                                                  motion: const ScrollMotion(),
                                                  children: [
                                                    SlidableAction(
                                                      onPressed:
                                                          (context) async {
                                                        if (_.hasConnection
                                                                .value ==
                                                            true) {
                                                          await _
                                                              .deleteTransaksi(
                                                                  valueId: _
                                                                      .transaksiItemList[
                                                                          index]
                                                                      .id!)
                                                              .whenComplete(
                                                            () {
                                                              _.transaksiItemList
                                                                  .removeAt(
                                                                      index);
                                                            },
                                                          );
                                                        } else {
                                                          Get.snackbar('Error',
                                                              'Check your internet connection');
                                                        }
                                                      },
                                                      backgroundColor:
                                                          errorColor,
                                                      foregroundColor:
                                                          whiteColor,
                                                      icon: Icons.delete,
                                                      spacing: 5,
                                                      label: 'Delete',
                                                    ),
                                                    SlidableAction(
                                                      onPressed: (context) {
                                                        bool isPengeluaran = (_
                                                                    .transaksiItemList[
                                                                        index]
                                                                    .pemasukan ==
                                                                null)
                                                            ? true
                                                            : false;

                                                        ListRekapTransaksi
                                                            listRekapTransaksi =
                                                            ListRekapTransaksi(
                                                          id: _
                                                              .transaksiItemList[
                                                                  index]
                                                              .id,
                                                          keterangan: (_
                                                                      .transaksiItemList[
                                                                          index]
                                                                      .keterangan ==
                                                                  '')
                                                              ? ''
                                                              : _
                                                                  .transaksiItemList[
                                                                      index]
                                                                  .keterangan,
                                                          pemasukan: (_
                                                                      .transaksiItemList[
                                                                          index]
                                                                      .pemasukan ==
                                                                  null)
                                                              ? null
                                                              : _
                                                                  .transaksiItemList[
                                                                      index]
                                                                  .pemasukan,
                                                          pengeluaran: (_
                                                                      .transaksiItemList[
                                                                          index]
                                                                      .pengeluaran ==
                                                                  null)
                                                              ? null
                                                              : _
                                                                  .transaksiItemList[
                                                                      index]
                                                                  .pengeluaran,
                                                          tanggal_edit: _
                                                              .transaksiItemList[
                                                                  index]
                                                              .tanggal_edit,
                                                        );

                                                        Get.toNamed(
                                                          Routes
                                                              .TransaksiUpdate,
                                                          arguments: [
                                                            isPengeluaran,
                                                            listRekapTransaksi,
                                                          ],
                                                        );
                                                      },
                                                      backgroundColor:
                                                          Colors.orangeAccent,
                                                      foregroundColor:
                                                          Colors.white,
                                                      icon: Icons.edit_rounded,
                                                      spacing: 5,
                                                      label: 'Update',
                                                    ),
                                                  ],
                                                ),
                                                child: ListTile(
                                                  dense: true,
                                                  leading:
                                                      (_.transaksiItemList[index]
                                                                  .pemasukan ==
                                                              null)
                                                          ? Icon(
                                                              Icons
                                                                  .trending_down_rounded,
                                                              color: errorColor,
                                                              size: 25,
                                                            )
                                                          : Icon(
                                                              Icons
                                                                  .trending_up_rounded,
                                                              color: greenColor,
                                                              size: 25,
                                                            ),
                                                  title: (_
                                                              .transaksiItemList[
                                                                  index]
                                                              .keterangan ==
                                                          null)
                                                      ? (_.transaksiItemList[index]
                                                                  .pemasukan ==
                                                              null)
                                                          ? Text(
                                                              'Pengeluaran',
                                                              style:
                                                                  darkTextFont
                                                                      .copyWith(
                                                                fontSize:
                                                                    sy(10),
                                                              ),
                                                            )
                                                          : Text(
                                                              'Pemasukan',
                                                              style:
                                                                  darkTextFont
                                                                      .copyWith(
                                                                fontSize:
                                                                    sy(10),
                                                              ),
                                                            )
                                                      : Text(
                                                          '${_.transaksiItemList[index].keterangan}',
                                                          style: darkTextFont
                                                              .copyWith(
                                                            fontSize: sy(10),
                                                          ),
                                                        ),
                                                  subtitle: Text(
                                                    '-',
                                                    style:
                                                        greyTextFont.copyWith(
                                                      fontSize: sy(8),
                                                    ),
                                                  ),
                                                  trailing: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      (_.transaksiItemList[index]
                                                                  .pemasukan ==
                                                              null)
                                                          ? Text(
                                                              Methods.convertToIdr(
                                                                  _.transaksiItemList[index]
                                                                          .pengeluaran ??
                                                                      0.0,
                                                                  0),
                                                              style:
                                                                  errorTextFont
                                                                      .copyWith(
                                                                fontSize: sy(9),
                                                              ),
                                                            )
                                                          : Text(
                                                              Methods.convertToIdr(
                                                                  _.transaksiItemList[index]
                                                                          .pemasukan ??
                                                                      0.0,
                                                                  0),
                                                              style:
                                                                  greenTextFont
                                                                      .copyWith(
                                                                fontSize: sy(9),
                                                              ),
                                                            ),
                                                      Text(
                                                        '${_.transaksiItemList[index].tanggal}',
                                                        style: greyTextFont
                                                            .copyWith(
                                                          fontSize: sy(8),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}
