import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:starter_1/models/rekap_transaksi.dart';
import 'package:starter_1/models/transaksi.dart';
import 'package:starter_1/modules/transaction/transaction_controller.dart';
import 'package:starter_1/shared/themes.dart';
import 'package:validators/validators.dart';

// ignore: must_be_immutable
class TransactionUpdateView extends StatelessWidget {
  TransactionUpdateView({Key? key}) : super(key: key);

  //* Variables
  bool? isPengeluaran;
  ListRekapTransaksi? transaksiValue;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    TransactionController _transactionController =
        Get.put(TransactionController());

    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return GetX<TransactionController>(
          initState: (_) {
            isPengeluaran = Get.arguments[0];
            transaksiValue = Get.arguments[1];

            _transactionController.currentDateValue =
                DateFormat('yyyy-MM-dd').parse(transaksiValue!.tanggal_edit!);

            _transactionController.txtTanggalController.value.text =
                DateFormat("yyyy-MM-dd")
                    .format(_transactionController.currentDateValue);

            _transactionController.txtNamaController.text =
                (transaksiValue!.keterangan == '' ||
                        transaksiValue!.keterangan == null)
                    ? ''
                    : transaksiValue!.keterangan!;

            if (isPengeluaran == true) {
              _transactionController.txtNominalController.text =
                  transaksiValue!.pengeluaran.toString();
            } else {
              _transactionController.txtNominalController.text =
                  transaksiValue!.pemasukan.toString();
            }
          },
          builder: (_) {
            return Scaffold(
              appBar: AppBar(
                centerTitle: true,
                title: Text('Update Transaksi'),
              ),
              body: Form(
                key: _formKey,
                child: InkWell(
                  onTap: () {
                    FocusScope.of(context).unfocus();
                  },
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  focusColor: Colors.transparent,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      vertical: defaultVerticalMargin,
                      horizontal: defaultHorizontalMargin,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Material(
                          elevation: 4,
                          borderRadius: BorderRadius.circular(4),
                          child: Container(
                            padding: EdgeInsets.only(
                              left: 10,
                              right: 10,
                              top: 10,
                              bottom: 30,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white,
                            ),
                            child: Column(
                              children: [
                                TextFormField(
                                  keyboardType: TextInputType.number,
                                  controller: _.txtNominalController,
                                  textInputAction: TextInputAction.done,
                                  validator: (value) {
                                    if (isNull(value)) {
                                      return 'Nominal harus di isi';
                                    } else if (double.parse(
                                            _transactionController
                                                .txtNominalController.text
                                                .split('.')
                                                .join("")) ==
                                        0) {
                                      return 'Nominal harus di isi';
                                    } else if (double.parse(
                                            _transactionController
                                                .txtNominalController.text
                                                .split('.')
                                                .join("")) >
                                        999999) {
                                      return 'Maksimal Nominal Rp 999.999';
                                    }
                                  },
                                  decoration: InputDecoration(
                                    alignLabelWithHint: true,
                                    labelText: 'Nominal',
                                    hintText: 'Masukkan nominal',
                                    isDense: true,
                                    errorStyle: errorTextFont.copyWith(
                                      fontSize: sy(8),
                                      height: 0.6,
                                    ),
                                    enabledBorder: enabledBorder,
                                    focusedBorder: focusedBorder,
                                    errorBorder: errorBorder,
                                    focusedErrorBorder: focusedError,
                                    labelStyle: secondBlackTextFont,
                                    floatingLabelStyle: secondBlackTextFont,
                                    hintStyle:
                                        greyTextFont.copyWith(fontSize: sy(10)),
                                  ),
                                ),
                                TextFormField(
                                  controller: _.txtNamaController,
                                  textInputAction: TextInputAction.done,
                                  onFieldSubmitted: (value) {
                                    FocusScope.of(context).unfocus();
                                  },
                                  validator: (value) {},
                                  decoration: InputDecoration(
                                    alignLabelWithHint: true,
                                    labelText: 'Nama Pemasukan',
                                    hintText: 'Masukkan nama pemasukan',
                                    isDense: true,
                                    enabledBorder: enabledBorder,
                                    focusedBorder: focusedBorder,
                                    labelStyle: secondBlackTextFont,
                                    floatingLabelStyle: secondBlackTextFont,
                                    hintStyle:
                                        greyTextFont.copyWith(fontSize: sy(10)),
                                  ),
                                ),
                                TextFormField(
                                  enableInteractiveSelection: false,
                                  readOnly: true,
                                  controller: _.txtTanggalController.value,
                                  onFieldSubmitted: (value) {},
                                  validator: (value) {},
                                  decoration: InputDecoration(
                                    alignLabelWithHint: true,
                                    labelText: 'Tanggal',
                                    hintText: 'Pilih Tanggal',
                                    isDense: true,
                                    enabledBorder: enabledBorder,
                                    focusedBorder: focusedBorder,
                                    focusedErrorBorder: focusedError,
                                    labelStyle: secondBlackTextFont,
                                    floatingLabelStyle: secondBlackTextFont,
                                    hintStyle: greyTextFont.copyWith(
                                      fontSize: sy(10),
                                    ),
                                    suffixIcon: Icon(
                                      Icons.arrow_right_rounded,
                                      size: 25,
                                      color: blackColor,
                                    ),
                                  ),
                                  onTap: () {
                                    showDatePicker(
                                      context: context,
                                      initialDate: _.currentDateValue,
                                      lastDate:
                                          (_.currentDateValue == DateTime.now())
                                              ? _.currentDateValue
                                              : DateTime.now(),
                                      firstDate: DateTime(2021),
                                      builder: (BuildContext? context,
                                          Widget? child) {
                                        return Theme(
                                          data: ThemeData.light().copyWith(
                                            primaryColor: mainColor,
                                            buttonTheme: ButtonThemeData(
                                              textTheme:
                                                  ButtonTextTheme.primary,
                                            ),
                                            colorScheme: ColorScheme.light(
                                              primary: mainColor,
                                            ).copyWith(secondary: blackColor),
                                          ),
                                          child: child!,
                                        );
                                      },
                                    ).then(
                                      (date) {
                                        if (date != null) {
                                          _.txtTanggalController.value.text =
                                              DateFormat('yyyy-MM-dd')
                                                  .format(date);
                                        }
                                      },
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                        Container(
                          padding: EdgeInsets.symmetric(
                            vertical: defaultVerticalMargin,
                            horizontal: defaultHorizontalMargin,
                          ),
                          height: sy(45),
                          width: double.infinity,
                          child: TextButton(
                            style: TextButton.styleFrom(
                              alignment: Alignment.center,
                              backgroundColor: mainColor,
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              primary: whiteColor,
                            ),
                            child: (_.isLoadingUpdateTransaksi.value == true)
                                ? Center(
                                    child: SizedBox(
                                      height: sy(20),
                                      width: sy(20),
                                      child: CircularProgressIndicator(
                                        color: whiteColor,
                                      ),
                                    ),
                                  )
                                : Text(
                                    "Update Transaksi",
                                    style: whiteTextFont.copyWith(
                                      fontSize: sy(12),
                                    ),
                                  ),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();

                                if (_.isLoadingUpdateTransaksi.value == false) {
                                  Transaksi finalTransaksiValue = Transaksi(
                                    id: transaksiValue!.id,
                                    jumlah_masuk: (isPengeluaran == true)
                                        ? null
                                        : double.parse(_
                                            .txtNominalController.text
                                            .split('.')
                                            .join("")),
                                    jumlah_keluar: (isPengeluaran == true)
                                        ? double.parse(_
                                            .txtNominalController.text
                                            .split('.')
                                            .join(""))
                                        : null,
                                    keterangan: _.txtNamaController.text,
                                    tanggal: _.txtTanggalController.value.text,
                                  );

                                  _.updateTransaksi(
                                    updatedTransaksi: finalTransaksiValue,
                                  );
                                }
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
