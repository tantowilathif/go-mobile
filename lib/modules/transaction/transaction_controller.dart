import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_masked_text2/flutter_masked_text2.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/models/rekap_transaksi.dart';
import 'package:starter_1/models/transaksi.dart';
import 'package:starter_1/modules/transaction/transaction_repository.dart';
import 'package:starter_1/utils/internet_connection_check.dart';

class TransactionController extends GetxController {
  //* Vairables for conditional view
  var isHarianPicked = false.obs;
  var isMingguanPicked = false.obs;
  var isBulananPicked = false.obs;
  var isCustomPeriodePicked = false.obs;
  var filterName = ''.obs;
  var customStartDate = ''.obs;
  var customEndDate = ''.obs;
  var isLoadingGetRekapTransaksi = false.obs;
  var isLoadingUpdateTransaksi = false.obs;
  var isCustomDidNotSelected = false.obs;
  var isGotTimeOut = false.obs;
  var hasConnection = true.obs;

  //* Text Editing Controllers
  var txtNominalController = MoneyMaskedTextController(
      precision: 0, decimalSeparator: "", initialValue: 0);
  TextEditingController txtNamaController = TextEditingController();
  Rx<TextEditingController> txtKategoriController = TextEditingController().obs;
  Rx<TextEditingController> txtTanggalController = TextEditingController().obs;

  //* Other Variables
  final TransactionRepository _transactionRepository = TransactionRepository();

  RekapTransaksi? rekapTransaksi;

  RxList<ListRekapTransaksi> transaksiItemList = <ListRekapTransaksi>[].obs;

  Rxn<double> pemasukan = Rxn<double>();
  Rxn<double> pengeluaran = Rxn<double>();
  Rxn<double> keuntungan = Rxn<double>();

  late DateTime currentDateValue;

  //* Cycles
  @override
  void onInit() async {
    super.onInit();

    hasConnection.value =
        await InternetConnectionCheck.getInstance().checkConnectivity();

    InternetConnectionCheck.getInstance().connectionChange.listen(
      (event) {
        connectionChanged(event);
      },
    );

    if (hasConnection.value == false) {
      isGotTimeOut.value = true;
    }
  }

  //* Methods (Function)
  void connectionChanged(dynamic hasConnectionStatus) {
    hasConnection.value = hasConnectionStatus;
  }

  void filterStatus(int value) {
    if (value == 0) {
      filterName.value = "Hari Ini";
      customStartDate.value = '';
      customEndDate.value = '';
      isMingguanPicked.value = false;
      isBulananPicked.value = false;
      isCustomPeriodePicked.value = false;
      isCustomDidNotSelected.value = false;

      var tanggalMulaiValue = DateFormat('yyyy-MM-dd').format(DateTime.now());
      var tanggalSelesaiValue = DateFormat('yyyy-MM-dd').format(DateTime.now());

      if (hasConnection.value == true) {
        getRekapTransaksi(
          tanggalMulai: tanggalMulaiValue,
          tanggalSelesai: tanggalSelesaiValue,
        );
      } else {
        Get.snackbar('Error', 'Check your internet connection');
        isLoadingGetRekapTransaksi.value = false;
      }
    } else if (value == 1) {
      filterName.value = "Minggu Ini";
      customStartDate.value = '';
      customEndDate.value = '';
      isHarianPicked.value = false;
      isBulananPicked.value = false;
      isCustomPeriodePicked.value = false;
      isCustomDidNotSelected.value = false;

      var now = DateTime.now();

      var tanggalMulaiValue = DateFormat('yyyy-MM-dd').format(
        DateTime(now.year, now.month, now.day - 7),
      );
      var tanggalSelesaiValue = DateFormat('yyyy-MM-dd').format(now);

      if (hasConnection.value == true) {
        getRekapTransaksi(
          tanggalMulai: tanggalMulaiValue,
          tanggalSelesai: tanggalSelesaiValue,
        );
      } else {
        Get.snackbar('Error', 'Check your internet connection');
        isLoadingGetRekapTransaksi.value = false;
      }
    } else if (value == 2) {
      filterName.value = "Bulan Ini";
      customStartDate.value = '';
      customEndDate.value = '';
      isHarianPicked.value = false;
      isMingguanPicked.value = false;
      isCustomPeriodePicked.value = false;
      isCustomDidNotSelected.value = false;

      var now = DateTime.now();

      var tanggalMulaiValue = DateFormat('yyyy-MM-dd').format(
        DateTime(now.year, now.month, now.day - 30),
      );
      var tanggalSelesaiValue = DateFormat('yyyy-MM-dd').format(now);

      if (hasConnection.value == true) {
        getRekapTransaksi(
          tanggalMulai: tanggalMulaiValue,
          tanggalSelesai: tanggalSelesaiValue,
        );
      } else {
        Get.snackbar('Error', 'Check your internet connection');
        isLoadingGetRekapTransaksi.value = false;
      }
    } else if (value == 3) {
      filterName.value = "Periode";
      customStartDate.value = '';
      customEndDate.value = '';
      isHarianPicked.value = false;
      isMingguanPicked.value = false;
      isBulananPicked.value = false;
    }
  }

  Future<void> getRekapTransaksi(
      {String? tanggalMulai, String? tanggalSelesai}) async {
    // Variables
    User? userData;

    isLoadingGetRekapTransaksi.value = true;

    transaksiItemList.clear();

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    try {
      rekapTransaksi = await _transactionRepository.rekapTransaksiRequest(
        token: userData.token,
        userId: userData.id,
        tanggalMulai: tanggalMulai,
        tanggalSelesai: tanggalSelesai,
      );

      if (rekapTransaksi!.data!.RekMasterTransaksi!.pemasukan == null) {
        pemasukan.value = 0;
      } else {
        pemasukan.value = rekapTransaksi!.data!.RekMasterTransaksi!.pemasukan;
      }

      if (rekapTransaksi!.data!.RekMasterTransaksi!.pengeluaran == null) {
        pengeluaran.value = 0;
      } else {
        pengeluaran.value =
            rekapTransaksi!.data!.RekMasterTransaksi!.pengeluaran;
      }

      if (rekapTransaksi!.data!.RekMasterTransaksi!.saldo == null) {
        keuntungan.value = 0;
      } else {
        keuntungan.value = rekapTransaksi!.data!.RekMasterTransaksi!.saldo;
      }

      transaksiItemList.value = rekapTransaksi!.data!.LapdetailTransaksi!;

      print(jsonEncode(transaksiItemList.toJson()));

      isGotTimeOut.value = false;
      isLoadingGetRekapTransaksi.value = false;
    } catch (e) {
      print(e.toString());

      isLoadingGetRekapTransaksi.value = false;

      if (e.toString() == "Connection Timeout Coba Lagi") {
        keuntungan.value = 0;
        isGotTimeOut.value = true;
        Get.snackbar('Timeout', 'Silahkan Coba Lagi');
      }

      // Get.snackbar('Error', 'Something wrong please try again');
    }
  }

  Future<void> updateTransaksi({Transaksi? updatedTransaksi}) async {
    isLoadingUpdateTransaksi.value = true;

    // Variables
    User? userData;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));
    if (hasConnection.value == true) {
      try {
        await _transactionRepository.updateTransaksi(
          token: userData.token,
          userId: userData.id,
          value: updatedTransaksi,
        );

        isLoadingUpdateTransaksi.value = false;

        Get.find<TransactionController>().filterStatus(0);

        Get.back();
      } catch (e) {
        print(e.toString());

        isLoadingUpdateTransaksi.value = false;

        if (e.toString() == "Connection Timeout Coba Lagi") {
          Get.snackbar('Timeout', 'Silahkan Coba Lagi');
        } else {
          Get.snackbar('Error', 'Something wrong please try again');
        }
      }
    } else {
      Get.snackbar('Error', 'Check your internet connection');

      isLoadingUpdateTransaksi.value = false;
    }
    update();
  }

  Future<void> deleteTransaksi({int? valueId}) async {
    // Variables
    User? userData;

    // Init shared preference
    SharedPreferences prefs = await SharedPreferences.getInstance();

    userData = User.fromJson(jsonDecode(prefs.getString('user')!));

    try {
      await _transactionRepository.deleteTransaksi(
        token: userData.token,
        valueId: valueId,
      );

      filterStatus(0);
    } catch (e) {
      print(e.toString());

      Get.snackbar('Error', 'Something wrong please try again');
    }

    update();
  }
}
