import 'package:json_annotation/json_annotation.dart';

part 'category.g.dart';

// ignore_for_file: non_constant_identifier_names

@JsonSerializable(explicitToJson: true)
class Category {
  CategoryList? data;
  String? message;
  String? status;
  int? status_code;

  Category({
    this.data,
    this.message,
    this.status,
    this.status_code,
  });

  factory Category.fromJson(Map<String, dynamic> parsedJson) =>
      _$CategoryFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CategoryList {
  int? total_items;
  List<CategoryItem>? list;

  CategoryList({this.total_items, this.list});

  factory CategoryList.fromJson(Map<String, dynamic> parsedJson) =>
      _$CategoryListFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$CategoryListToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CategoryItem {
  int? id;
  int? user_id;
  String? tipe;
  String? nama;
  bool? selected;

  CategoryItem({this.id, this.user_id, this.tipe, this.nama, this.selected});

  factory CategoryItem.fromJson(Map<String, dynamic> parsedJson) =>
      _$CategoryItemFromJson(parsedJson);

  Map<String, dynamic> toJson() => _$CategoryItemToJson(this);
}
