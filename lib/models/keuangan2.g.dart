// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'keuangan2.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Keuangan2 _$Keuangan2FromJson(Map<String, dynamic> json) => Keuangan2(
      data: json['data'] == null
          ? null
          : KeuanganItem2.fromJson(json['data'] as Map<String, dynamic>),
      message: json['message'] as String?,
      status: json['status'] as String?,
      status_code: json['status_code'] as int?,
    );

Map<String, dynamic> _$Keuangan2ToJson(Keuangan2 instance) => <String, dynamic>{
      'data': instance.data?.toJson(),
      'message': instance.message,
      'status': instance.status,
      'status_code': instance.status_code,
    };

KeuanganItem2 _$KeuanganItem2FromJson(Map<String, dynamic> json) =>
    KeuanganItem2(
      pemasukan: (json['pemasukan'] as num?)?.toDouble(),
      pengeluaran: (json['pengeluaran'] as num?)?.toDouble(),
      saldo: (json['saldo'] as num?)?.toDouble(),
      total: (json['total'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$KeuanganItem2ToJson(KeuanganItem2 instance) =>
    <String, dynamic>{
      'pemasukan': instance.pemasukan,
      'pengeluaran': instance.pengeluaran,
      'saldo': instance.saldo,
      'total': instance.total,
    };
