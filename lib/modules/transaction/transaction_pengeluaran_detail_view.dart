import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:relative_scale/relative_scale.dart';
import 'package:starter_1/models/rekap_transaksi.dart';
import 'package:starter_1/shared/themes.dart';

// ignore: must_be_immutable
class TransactionPengeluaranDetail extends StatelessWidget {
  TransactionPengeluaranDetail({Key? key}) : super(key: key);

  List<ListRekapTransaksi> listRekapTransaksi = Get.arguments[0];

  @override
  Widget build(BuildContext context) {
    return RelativeBuilder(
      builder: (context, height, width, sy, sx) {
        return Scaffold(
          body: SafeArea(
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: defaultVerticalMargin,
                horizontal: defaultHorizontalMargin,
              ),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Pengeluaran ${Get.arguments[1]}',
                      style: darkTextFont.copyWith(
                        fontSize: sy(12),
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.separated(
                      itemCount: listRekapTransaksi.length,
                      separatorBuilder: (context, index) {
                        return Divider(
                          height: 0,
                          color: greyColor,
                          // thickness: 0.4,
                        );
                      },
                      itemBuilder: (context, index) {
                        return ListTile(
                          dense: true,
                          leading: Icon(
                            Icons.trending_down_rounded,
                            color: errorColor,
                            size: 25,
                          ),
                          title: Text(
                            listRekapTransaksi[index].keterangan ??
                                "Pengeluaran",
                            style: darkTextFont.copyWith(
                              fontSize: sy(10),
                            ),
                          ),
                          subtitle: Text(
                            '-',
                            style: greyTextFont.copyWith(
                              fontSize: sy(8),
                            ),
                          ),
                          trailing: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                '${listRekapTransaksi[index].pengeluaran ?? listRekapTransaksi[index].pemasukan}',
                                style: errorTextFont.copyWith(
                                  fontSize: sy(8),
                                ),
                              ),
                              Text(
                                '${listRekapTransaksi[index].tanggal}',
                                style: greyTextFont.copyWith(
                                  fontSize: sy(8),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
