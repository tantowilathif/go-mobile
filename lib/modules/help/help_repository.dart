import 'package:dio/dio.dart';
import 'package:starter_1/models/bantuan.dart';
import 'package:starter_1/shared/constant.dart';

class HelpRepository {
  //* Variables
  Dio dioService = Dio();

  Future<Bantuan?> listCategoryRequest({String? token}) async {
    // Variables
    Response response;

    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };

    BaseOptions options = BaseOptions(
      headers: headers,
    );

    try {
      response = await Dio(options).get(
        "$API/bantuan/list",
      );

      return Bantuan.fromJson(response.data);
    } on DioError catch (e) {
      print("[ERROR] - Get List Bantuan == ${e.toString()}");

      Bantuan bantuan = Bantuan.fromJson(e.response!.data);

      throw bantuan.message!;
    }
  }
}
