import 'package:dio/dio.dart';
import 'package:starter_1/models/auth.dart';
import 'package:starter_1/shared/constant.dart';

class LoginRepository {
  //* Variables
  Dio dioService = Dio();

  Future<Auth?> loginRequest({User? user}) async {
    // Variables
    Response response;

    try {
      BaseOptions options = BaseOptions(
        connectTimeout: 10000,
      );

      response = await Dio(options).post(
        "$API/login",
        data: user!.toJson(),
      );

      return Auth.fromJson(response.data);
    } on DioError catch (e) {
      print("[ERROR] - Login Account == ${e.toString()}");

      if (e.type == DioErrorType.connectTimeout) {
        throw "Connection Timeout Coba Lagi";
      } else {
        Auth auth = Auth.fromJson(e.response!.data);

        throw auth.message!;
      }
    }
  }
}
